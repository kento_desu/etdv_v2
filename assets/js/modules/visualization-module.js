Visualization = function () {
    var controller    = base_url+'visualization/';
    var property      = {};

    var fn = function() {

        function change_category()
        {
            $('.metric-category').off('click');
            $('.metric-category').on('click', function() {
                var category = $(this).find('input').attr('id');

                switch(category)
                {
                    case 'nof-category':
                        chart.nof();
                    break;
                    case 'dff-category':
                        chart.dff();
                    break;
                    case 'tct-category':
                        chart.tct();
                    break;
                    default:
                        chart.nof();
                    break;
                }
                
            });
        }

        function filter_listener() {
            $('.chart-action').off('click');
            $('.chart-action').on('click', function() {
                var category = $('.metric-group > .active > input').attr('id');
                switch(category)
                {
                    case 'nof-category':
                        chart.nof();
                    break;
                    case 'dff-category':
                        chart.dff();
                    break;
                    case 'tct-category':
                        chart.tct();
                    break;
                    default:
                        chart.nof();
                    break;
                }

            });
        }

        function export_pdf() {
            $('.export-pdf').off('click');
            $('.export-pdf').on('click', function() {
                
                var aoi = {};
                $.ajax({
                    url     : controller + "get_aoi",
                    type    : 'GET',
                    dataType: 'json',
                    success : function(result) {
                        aoi = result;
                    },
                    complete: function() {
                        var newCanvas    = document.querySelector('#summary-chart');
                        var newCanvasImg = newCanvas.toDataURL("image/png", 1.0);
          
                        var doc          = new jsPDF();
                        var y            = 15;
                        doc.setFontSize(25);
                        doc.text(30, y, 'Stimulus Report: ' + $('h4.title').text());
                        y = add_vertical_margin(doc,y, 20);

                        doc.setFontSize(15);
                        doc.text(15, y, 'Stimulus Name:');
                        doc.text(60, y, 'Description:');
                        doc.text(120, y, 'Date:');

                        y = add_vertical_margin(doc,y);
                        doc.setFontSize(13);
                        doc.text(20, y, $('span.stimuli-name').text());
                        doc.text(65, y, $('span.stimuli-desc').text());
                        doc.text(125, y, $('span.stimuli-date').text());

                        y = add_vertical_margin(doc,y);
                        doc.setFontSize(15);
                        doc.text(15, y, 'Visualization');

                        y = add_vertical_margin(doc,y);
                        doc.addImage(newCanvasImg, 'PNG', 15, y, 185, 100 );

                        y = add_vertical_margin(doc, y, 15, 100);
                        doc.text(15, y, 'Area of Interests');
                        doc.setFontSize(10);
                        y = add_vertical_margin(doc, y);

                        for(var i = 0; i < aoi.length; i++)
                        {
                            doc.text(20, y, 'AOI' + (i + 1));
                            doc.text(40, y, 'P1 ('+aoi[i]['x1']+', '+aoi[i]['y1']+')');
                            doc.text(80, y, 'P2 ('+aoi[i]['x2']+', '+aoi[i]['y2']+')');
                            doc.text(120, y, 'P3 ('+aoi[i]['x3']+', '+aoi[i]['y3']+')');
                            doc.text(160, y, 'P4 ('+aoi[i]['x4']+', '+aoi[i]['y4']+')');
                            doc.line(15, y + 1, 185, y + 1);
                            y = add_vertical_margin(doc, y, 5);
                        }

                        y = add_vertical_margin(doc, y, 15);
                        doc.setFontSize(15);
                        doc.text(15, y, 'Participants:');
                        doc.setFontSize(10);
                        y     = add_vertical_margin(doc, y);
                        var x = 20;

                        $('.chart-action:checked').each(function() {
                            
                            $label = $('label[for="'+ $(this).attr('id') +'"]');
                            if ($label.length > 0 ) {
                                doc.text(x, y, $label.text());
                                x += 40;
                            }
                            if(x > 185 - 20)
                            {
                                doc.line(15, y + 1, 185, y + 1);
                                y = add_vertical_margin(doc, y, 5);
                                x = 20;
                            }
                        });
                        doc.line(15, y + 1, 185, y + 1);
                        doc.save('new-canvas.pdf');
                    }
                });

                
            });
        }

        function add_vertical_margin(doc, y, padding, height)
        {
            var adjusted_height  = height  || 0;
            var adjusted_padding = padding || 10;
            var adjusted_y       = y + adjusted_padding + adjusted_height;
            pageHeight           = doc.internal.pageSize.height - 20;

            if (adjusted_y >= pageHeight)
            {
              doc.addPage();
              adjusted_y = 15 // Restart height position
            }

            return adjusted_y;
        }

        return {
            filter_listener: filter_listener,
            change_category: change_category,
            export_pdf     : export_pdf
        }
    }();

    var chart = function() {
        function nof()  
        {
            $('h4.title').html('Number of Fixations');
            $("canvas#summary-chart").remove();
            $("div.chart-container").append('<canvas id="summary-chart" width="400" height="200"></canvas>');

            Visualization.property.participants_ids = [];
            $('input.chart-action').each(function(i, obj) {
                if($(obj).is(':checked'))
                {
                    Visualization.property.participants_ids.push($(obj).attr('data-id'));
                }
            });

            $.ajax({
                url    : controller + "get_nof",
                type   : 'GET',
                data   : Visualization.property,
                dataType: 'json',
                success: function(result) {
                    var ctx = document.getElementById("summary-chart").getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Areas Of Interest"],
                            datasets: chart.structure_dataset(result,'nof')
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    },
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Number of Fixations'
                                      }
                                }]
                            }
                        }
                    });
                },
                complete: function() {
                    export_btn_callback();
                }
            });
        }

        function dff()
        {
            $('h4.title').html('Duration of First Fixations');
            $("canvas#summary-chart").remove();
            $("div.chart-container").append('<canvas id="summary-chart" width="400" height="200"></canvas>');

            Visualization.property.participants_ids = [];
            $('input.chart-action').each(function(i, obj) {
                if($(obj).is(':checked'))
                {
                    Visualization.property.participants_ids.push($(obj).attr('data-id'));
                }
            });

            $.ajax({
                url    : controller + "get_dff",
                type   : 'GET',
                data   : Visualization.property,
                dataType: 'json',
                success: function(result) {
                    var ctx = document.getElementById("summary-chart").getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Areas Of Interest"],
                            datasets: chart.structure_dataset(result,'dff')
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    },
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Duration of First Fixation'
                                      }
                                }]
                            }
                        }
                    });
                },
                complete: function() {
                    export_btn_callback();
                }
            });
        }

        function tct()
        {
            $('h4.title').html('Total Contact Time');
            $("canvas#summary-chart").remove();
            $("div.chart-container").append('<canvas id="summary-chart" width="400" height="200"></canvas>');

            Visualization.property.participants_ids = [];
            $('input.chart-action').each(function(i, obj) {
                if($(obj).is(':checked'))
                {
                    Visualization.property.participants_ids.push($(obj).attr('data-id'));
                }
            });

            $.ajax({
                url    : controller + "get_tct",
                type   : 'GET',
                data   : Visualization.property,
                dataType: 'json',
                success: function(result) {
                    var ctx = document.getElementById("summary-chart").getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Areas Of Interest"],
                            datasets: chart.structure_dataset(result,'tct')
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    },
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Total Contact Time'
                                      }
                                }]
                            }
                        }
                    });
                },
                complete: function() {
                    export_btn_callback();
                }
            });
        }

        function export_btn_callback()
        {
            if($('.chart-action:checked').length > 0) {
                $('label.export-pdf').removeAttr('disabled');
                fn.export_pdf();
            }
            else {
                $('label.export-pdf').attr("disabled", "disabled");
            }
            
        }

        function structure_dataset(data, metric)
        {
            var datasets      = [];
            var color         = 1;
            var color_divider = Math.round(360 / Object.keys(data).length);

            for(var i = 0; i <  Object.keys(data).length; i++)
            {
                var dataset             = {};
                dataset.label           = 'AOI ' + (i + 1);
                dataset.data            = [data[i][metric]];
                dataset.backgroundColor = ["hsl(" + (1 * color) + ",20%,50%)"];
                dataset.borderColor     = ["hsl(" + (1 * color) + ",100%,50%)"];
                dataset.borderWidth     = 1;
                datasets.push(dataset);
                color += color_divider;

            }
            return datasets;
        }

        return {
            nof              : nof,
            dff              : dff,
            tct              : tct,
            structure_dataset: structure_dataset
        }

    }();

    return {
        property: property,
        fn      : fn,
        chart   : chart
    }

}();