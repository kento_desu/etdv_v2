Participant = function() {
    var property           = {};
    var $table             = {},
        $fixation_table    = {},
        $file_upload_input = {};

    function config() {
        $table             = $('table.participant');
        $fixation_table    = $('table.fixation-data');
        $file_upload_input = $('input#file-upload');
    }

    var fn = function(){
        function row_selection_listener()
        {
            config();
            $table.find('tbody').on('click', 'tr', function () {
                
                if($(this).hasClass('selected')) {
                    Participant.property.participant_id = $(this).find('td').eq(0).text();
                } else {
                    delete Participant.property.participant_id;
                }
                table.participant_fixation();

            } );
        }

        function upload_success_callback()
        {
            config();
            $file_upload_input.on('filebatchuploadcomplete', function(event, files, extra) {
                $table.DataTable().draw(false);
            });
        }

        return {
            row_selection_listener : row_selection_listener,
            upload_success_callback: upload_success_callback
        }
    }();

    var table = function() {
        function all()
        {
            config();
            $table.DataTable().destroy();
            $table.dataTable({
                "sPaginationType": "full_numbers",
                dom              : 'Bfrtip',        // Needs button container
                select           : 'single',
                responsive       : true,
                processing       : true,
                serverSide       : true,
                keys             : true,
                autoWidth        : true,
                pageLength       : 10,
                columns          : [
                    {"data" : "participant_id"},
                    {"data" : "name"}
                ],
                ajax             : {
                    url    : base_url + "Participant/get_all",
                    type   : "get",
                    data   : function(data_sent) {
                        data_sent.stimuli_id = Participant.property.stimuli_id;
                    },
                    dataSrc: function(response) {                            
                        return response.data;
                    }
                },
                columnDefs: [
                     {
                        
                        // targets: 0,
                        // visible: false
                        // render: function (data) { return data+"  hi"; }
                    }],  
                buttons: 
                [
                    {
                        text  : '<i class="material-icons">add_circle_outline</i> Add New Participant/s',
                        name  : 'addstim',
                        action: function ( e, dt, button, config ) {
                            modal.add_participant();
                            /* $('#stimuliModal').modal({
                                show    : true,
                                keyboard: false,
                                backdrop: 'static'
                            }); */
                        }
                    },
                    {
                        // extend: 'selected', // Bind to Selected row
                        text  : '<i class = "material-icons">delete_forever</i> Delete',
                        name  : 'delete',      // do not change name
                        enabled: false,
                        action: function ( e, dt, button, config ) {
                        
                            /* $('input[name="opt"]').val('delete');
                            $('input[name="no"]').val(dt.row( { selected: true } ).data()[0]);
                            $('input[name="pname"]').val(dt.row( { selected: true } ).data()[1]);
                            $('input[name="desc"]').val(dt.row( { selected: true } ).data()[2]);
                            $('input[name="x"]').val(dt.row( { selected: true } ).data()[3]);
                            $('input[name="y"]').val(dt.row( { selected: true } ).data()[4]);
                            $('#submitForm').html('Delete');
                            $('.modal-title').html('Are you sure deleting this project?');
                            $('#editor-modal').modal('show'); */
                        }
                    }
                ],
                drawCallback: function(settings) {
                    fn.row_selection_listener();
                }
            });
        }

        function participant_fixation()
        {
            config();
            $fixation_table.DataTable().destroy();
            $fixation_table.dataTable({
                "sPaginationType": "full_numbers",
                dom              : 'Bfrtip',        // Needs button container
                //select           : false,
                responsive       : true,
                processing       : true,
                serverSide       : true,
                keys             : true,
                searching        : false,
                autoWidth        : true,
                pageLength       : 10,
                ordering         : false,
                columns          : [
                    {"data" : "x"},
                    {"data" : "y"},
                    {"data" : "duration", "className" : "text-center"}
                ],
                ajax             : {
                    url    : base_url + "Participant/get_fixation",
                    type   : "get",
                    data   : function(data_sent) {
                        data_sent.participant_id = Participant.property.participant_id || null;
                    },
                    dataSrc: function(response) {                            
                        return response.data;
                    }
                },
                buttons: [
                    {
                        text     : '<i class="material-icons">save_alt</i> Download CSV',
                        name     : 'addstim',
                        action   : function ( e, dt, button, config ) {
                            var participant_id = Participant.property.participant_id || '';
                            window.open(base_url + 'participant/download-csv/' + participant_id, '_blank');
                        }
                    }
                ]
               
            });
        }

        return {
            all                 : all,
            participant_fixation: participant_fixation
        }
    }();

    var modal = function() {
        function add_participant()
        {
            $.ajax({
                url: base_url + "render/add_participant_modal",
                dataType: 'html',
                success: function(response) {
                    show_modal(response);
                },
                complete: function() {
                    config();
                    var test = $file_upload_input.fileinput({
                        allowedFileExtensions: ["csv"],
                        browseOnZoneClick    : true,
                        //showBrowse           : false,
                        multiple: true,
                        uploadAsync          : true,
                        uploadUrl            : base_url + "participant/add_participant"
                    });

                    fn.upload_success_callback();
                }
            });
        }

        return {
            add_participant: add_participant
        }
    }();

    return {
        property: property,
        fn      : fn,
        table   : table,
        modal   : modal
    }
}();