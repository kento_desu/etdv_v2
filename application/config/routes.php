<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'render/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;
$config['allow_get_array']       = TRUE;
$config['enable_query_strings'] = TRUE;

$route['upload-file'] = 'upload/ajax_upload' ;

$route['login'] = 'render/login';
$route['register'] = 'render/register';
$route['dashboard'] = 'render/dashboard';
$route['profile'] = 'render/profile';
$route['projects'] = 'render/projects';
$route['stimuli'] = 'render/stimuli';
$route['stimuli_sharedfilepage'] = 'render/stimuli_sharedfilepage';
$route['participants'] = 'render/participant';
$route['shared_files'] = 'render/shared_files';
$route['connection'] = 'render/connection';
$route['parse'] = 'render/parse';
$route['connection-request'] = 'render/connection';
$route['visualization'] = 'render/visualize';

$route['login_ifexist'] = 'common/login_ifexist';
$route['logout'] = 'common/logout';
$route['session'] = 'common/insert_session';
$route['get-profile'] = 'profile/select_current_profile';
$route['update-profile'] = 'profile/update_profile';
$route['share-connection'] = 'project/share_connection';
$route['grant-ifexist'] = 'project/grant_ifexist';
$route['update-grant'] = 'project/updateGrant';


$route['insert_register'] = 'common/insert_register';
// $route['dashboard'] = 'render/dashboard';

