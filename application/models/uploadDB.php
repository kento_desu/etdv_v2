<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class uploadDB extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('utils');
    }

    public function insert($data)
    {
        // $data['researcher_rid'] = $this->utils->decrypt_id($this->session->rid);
        $this->db->insert('files', $data);
        return $this->db->insert_id();
    }

    public function update($data,$id)
    {
        // $data['researcher_rid'] = $this->utils->decrypt_id($this->session->rid);

        $this->db->where('files.researcher_rid', $id);
        $this->db->update('files', $data);
        return $this->db->insert_id();
    }

      public function ifExistFile($file)
    {
        $this->db->select('file_hash,file_name,file_ext,file_version');
        $this->db->from('files');
        $this->db->where('file_hash', $file);
        $this->db->order_by("file_version", "desc");
        $query = $this->db->get();

        if($query->num_rows() > 0){
            
             //return $query->row('file_name');
 
        $data['file_hash'] = $query->row('file_hash');
        $data['file_name'] = $query->row('file_name');
        $data['file_ext']  = $query->row('file_ext');
        $data['file_version']  = $query->row('file_version');
            
    
       $dataset[] = $data;
      
      // encoding array to json format
return ($dataset);



        }else{
            return FALSE;
        }

    }

}