<?php

defined('BASEPATH') OR exit('No direct script access allowed');

 class etdv_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('utils');
    }

    public function error_output($db_error){
        $data = array();

        $data['status'] = false;
        $data['error'] = $db_error['code'];
        $data['message'] = $db_error['message'];

        return $data;
    }

    public function output_template1($db_error, $affected_rows, $fetch = array())
    {
        $result = array();

        if ($db_error['code'] == 0 ) {
            if ($affected_rows > 0) {
                $result['status'] = true;
                if(!empty($fetch))  {
                    $result['data_fetch'] = $fetch;
                }
              }else {
                $result['status'] = false;
              }
            
        }else{
           $resut = $this->error_output($db_error);
        }
        return $result;
    }

    
     
    public function insert_register($params)
    {
        $res = array();
        $this->db->insert('researcher', $params);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        $res['id'] = $last_insert;
        return $res;
    }

    public function share_connection($params)
    {
        $res = array();
        $this->db->insert('share_projects', $params);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        $res['id'] = $last_insert;
        return $res;
    }

    public function total_projects()
    {
        $this->db->select('count(pid) as projects');
        $this->db->from('project');

        $this->db->where('researcher_rid = '. $this->utils->decrypt_id($this->session->rid));
        $this->db->where('row_status = 1');
        $query = $this->db->get();
        $db_error = $this->db->error();
        
        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }

    public function grant_ifexist($trid,$gs,$pid)
    {
        $this->db->select('*');
        $this->db->from('share_projects');
        $this->db->where('researcher_rid = '. $this->utils->decrypt_id($this->session->rid));
        $this->db->where('to_rid = '. $trid);
        $this->db->where('project_pid = '. $pid);
        $this->db->where('status', 1);
        
        $query = $this->db->get();
        $db_error = $this->db->error();
        
        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }

    public function get_image_data()
    {
        $this->db->select('md5(file_name) as imgname, fname, mname, lname')
         ->from('files')
         ->join('researcher', 'files.researcher_rid = researcher.rid')
         ->where('researcher.rid ='. $this->utils->decrypt_id($this->session->rid))
         ->where('files.row_status ='. 1)
         ;
        $query = $this->db->get();
        $db_error = $this->db->error();

        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }

    public function get_imageto_data($id)
    {
        $this->db->select('md5(file_name) as imgname, fname, mname, lname')
         ->from('files')
         ->join('researcher', 'files.researcher_rid = researcher.rid')
         ->where('researcher.rid ='. $id)
         ->where('files.row_status ='. 1);

        $query = $this->db->get();
        $db_error = $this->db->error();

        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }


     public function get_sharedproject_data($id)
    {
        $this->db->select('grant_status as status')
         ->from('share_projects')
         ->join('researcher', 'researcher.rid = share_projects.to_rid')
         ->where('researcher.rid ='. $this->utils->decrypt_id($this->session->rid))
         ->where('share_projects.project_pid =', $id);
        $query = $this->db->get();
        $db_error = $this->db->error();

        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }



    public function get_user_data()
    {
        $this->db->select('rid, fname, mname, lname')
        ->from('researcher')
        ->where('researcher.rid ='. $this->utils->decrypt_id($this->session->rid));
        $query = $this->db->get();
        $db_error = $this->db->error();

        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }

    


    public function total_connections()
    {
        $this->db->select('count(cid) as connections');
        $this->db->from('connections');

        $this->db->where('researcher_rid = '. $this->utils->decrypt_id($this->session->rid));
        $this->db->where('status = 1');
        $query = $this->db->get();
        $db_error = $this->db->error();
        
        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }


    public function total_stimuli()
    {
        
        $this->db->select('count(sid) as stimuli')
         ->from('project')
         ->join('stimuli', 'project.pid = stimuli.project_pid')
         ->where('project.researcher_rid ='. $this->utils->decrypt_id($this->session->rid));
        $query = $this->db->get();
        $db_error = $this->db->error();
        
        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }

    public function total_shared_files()
    {
        
        $this->db->select('count(shid) as shared_files')
         ->from('share_projects')
         ->where('share_projects.researcher_rid ='. $this->utils->decrypt_id($this->session->rid));
        $query = $this->db->get();
        $db_error = $this->db->error();
        
        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }

    public function total_request_connection()
    {
        
        $this->db->select('count(cid) as conn')
         ->from('connections')
         // ->join('stimuli', 'project.pid = stimuli.project_pid')
         ->where('connections.request_id ='. $this->utils->decrypt_id($this->session->rid))
         ->where('status = 0');

        $query = $this->db->get();
        $db_error = $this->db->error();
        
        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }

    public function insert_session($params){

        $this->db->insert('session', $params);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();
        
        $res = $this->utils->model_output($db_error, $affected_rows, null, null, $this->db->last_query());
        $res['id'] = $last_insert;
        return $res;
    }

     public function insertProject($params)
    {
        $res = array();
        $this->db->insert('project', $params);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        $res['id'] = $last_insert;
        return $res;
    }

    public function addStimulusInfo($params)
    {
        $res = array();
        $this->db->insert('stimuli', $params);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        $res['id'] = $last_insert;
        return $res;
    }

    public function updateStimulusInfo($params, $where)
    {
        $res = array();
        $this->db->where('sid', $where);
        $this->db->update('stimuli', $params);
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
    }

    public function addAOI($coordinates)
    {

        $this->db->insert('aoi', $coordinates);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        $res['id'] = $last_insert;
        return $res;
    }

    public function insert_fixation_file($params)
    {

        $this->db->insert('participant', $params);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        $res['id'] = $last_insert;
        // $this->session->set_userdata('ppid', $last_insert);
        return $res;
    }

    public function insert_fixation($data) {
        $this->db->insert_batch('fixation', $data);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        $res['id'] = $last_insert;
        return $res;
    }

    public function insertConnection($data) {
        $this->db->insert('connections', $data);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        $res['id'] = $last_insert;
        return $res;
}

    public function updateStimulus($data, $where)
    {
        $res = array();
        $this->db->where('sid', $where);
        $this->db->update('stimuli', $data);

        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
    }

    public function updateGrant($data, $to_rid, $pid)
    {
        $res = array();
        $this->db->where('researcher_rid', $this->utils->decrypt_id($this->session->rid));
        $this->db->where('to_rid', $to_rid);
        $this->db->where('project_pid', $pid);
        
        $this->db->update('share_projects', $data);

        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
    }

    public function updateParticipantFile($data, $where)
    {
        $res = array();
        $this->db->where('ppid', $where);
        $this->db->update('participant', $data);

        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
    }

    public function logout_session($data, $id){

        $this->db->where("ssid", $id);
        $this->db->update('session', $data);
        $last_insert = $this->db->insert_id();
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();
        
        $res = $this->utils->model_output($db_error, $affected_rows, null, null, $this->db->last_query());
        $res['id'] = $last_insert;
        return $res;
    }

    public function login_ifexist($email)
    {
        $this->db->select('rid, password');
        $this->db->from('researcher');
        $this->db->where('email', $email);
        $query = $this->db->get();
        $db_error = $this->db->error();
        $encrypt = ['rid'];
        return $this->utils->model_output($db_error, $query->num_rows(), $query->result(), $encrypt, $this->db->last_query());
    }

    public function connection_ifexist($req_id)
    {
        $this->db->select('request_id');
        $this->db->from('connections');
        $this->db->where('request_id', $req_id);
        $this->db->where('researcher_rid',$this->utils->decrypt_id($this->session->rid));
        $query = $this->db->get();
        $db_error = $this->db->error();
        // $encrypt = ['id'];
        return $this->utils->model_output($db_error, $query->num_rows(), $query->result(),null, $this->db->last_query());
    }

    public function stimulusId_ifexist($sid)
    {
        $this->db->select('*');
        $this->db->from('stimuli');
        $this->db->where('sid', $sid);
        $query = $this->db->get();
        $db_error = $this->db->error();
        $encrypt = ['sid'];
        return $this->utils->model_output($db_error, $query->num_rows(), $query->result(), $encrypt, $this->db->last_query());
    }


    public function aoi_ifexist($sid)
    {
        $this->db->select('*');
        $this->db->from('aoi');
        $this->db->where('stimuli_sid', $this->session->stimuli_id);
        $this->db->where('idaoi', $this->session->aoi_id);
        $query = $this->db->get();
        $db_error = $this->db->error();
        $encrypt = ['idaoi'];
        return $this->utils->model_output($db_error, $query->num_rows(), $query->result(), $encrypt, $this->db->last_query());
    }

    public function projectId_ifexist($pid)
    {
        $this->db->select('*');
        $this->db->from('project');
        $this->db->where('pid', $pid);
        $query = $this->db->get();
        $db_error = $this->db->error();
        $encrypt = ['pid'];
        return $this->utils->model_output($db_error, $query->num_rows(), $query->result(), $encrypt, $this->db->last_query());
    }

    public function select_current_profile(){

        $this->db->select('*');
        $this->db->from('researcher');
        $this->db->where('rid', $this->utils->decrypt_id($this->session->rid));
        
        $query = $this->db->get();
        $db_error = $this->db->error();
        $encrypt = ['rid'];
        return $this->utils->model_output($db_error, $query->num_rows(), $query->result(), null, $this->db->last_query());
    
    }
    
    public function update_profile($param, $pass){

        if($pass){

        $this->db->where('rid', $this->utils->decrypt_id($this->session->rid));
        $this->db->where('password', md5($pass));
        $this->db->update('researcher', $param);
    
        }else{
        
        $this->db->where('rid', $this->utils->decrypt_id($this->session->rid));
        $this->db->update('researcher', $param);
        }

        return $this->db->affected_rows();
    }

    function getConnectionDropdown()
    {

        $this->db->select('rid , fname, lname, mname');
        $this->db->from('researcher');
        $this->db->join('connections', 'researcher.rid = connections.request_id');
        $this->db->where("connections.researcher_rid =". $this->utils->decrypt_id($this->session->rid));
        $query = $this->db->get();
        $db_error = $this->db->error();

        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }

    function getConnectionShare($pid)
    {
        
        $this->db->select('*');
        $this->db->from('researcher');
        $this->db->join('share_projects', 'researcher.rid = share_projects.to_rid');
        $this->db->where("share_projects.researcher_rid =". $this->utils->decrypt_id($this->session->rid));
        $this->db->where("share_projects.project_pid =". $pid );
        $this->db->where('status', 1);
        $query = $this->db->get();
        $db_error = $this->db->error();

        $result = $this->output_template1($db_error, $query->num_rows(), $this->utils->o2a($query->result()));

        return $result;
    }


    //datatables
     public function select_projectList()
    {   
      /* table name from your mysql database */
        $table = 'project';

        /* column name of primary keys from your table */
        $primaryKey = 'pid';

        /* select the columns to fetch data from your table */
        
        $columns = array(
            array(
                'db'    => 'pid',
                'dt'    => 0,
                'field' => 'pid',
            ),
            array(
                'db'    => 'pname',
                'dt'    => 1,
                'field' => 'pname',
            ),
            array(
                'db'    => 'pdesc',
                'dt'    => 2,
                'field' => 'pdesc',
            ),
               array(
                'db'    => 'date_created',
                'dt'    => 3,
                'field' => 'date_created',
            ),
            // array(
            //     'db'    => 'date_modified',
            //     'dt'    => 4,
            //     'field' => 'date_modified',
            // ),
            array(
                'db'    => 'row_status',
                'dt'    => 4,
                'field' => 'row_status',
            ),
           
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname,
        );

        /* below are optional clauses only if you have more things to filter */

        /* put join clause, ex. $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id"  */

        // $joinQuery = "FROM branch_members 
        //                  JOIN member on member.idmember = branch_members.member_idmember 
        //                  JOIN branch on branch.idbranch = branch_members.branch_idbranch";

        $joinQuery ="";
        // $joinQuery = " FROM branch_members JOIN member on member.idmember = branch_members.member_idmember ";
        /* put extra where clause only if you have other specific data to find, ex. $extraWhere = "AND fname = 'John' AND lname = 'Wick'" */
        // $extraWhere = "status = 'Approved' || status = 'Denied'";
        $extraWhere = "row_status = 1 AND researcher_rid = ". $this->utils->decrypt_id($this->session->rid);

        /* put group clause, ex. $group_by = "age" */
        $group_by = "";


        /* this will return the data in json format */
        return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $group_by);

    }

    public function selectStimuliList()
    {   
      /* table name from your mysql database */
        $table = 'stimuli';

        /* column name of primary keys from your table */
        $primaryKey = 'sid';

        /* select the columns to fetch data from your table */
        
        $columns = array(
            array(
                'db'    => 'sid',
                'dt'    => 0,
                'field' => 'sid',
            ),
            array(
                'db'    => 'sname',
                'dt'    => 1,
                'field' => 'sname',
            ),
            array(
                'db'    => 'sdesc',
                'dt'    => 2,
                'field' => 'sdesc',
            ), array(
                'db'    => 'x_resolution',
                'dt'    => 3,
                'field' => 'x_resolution',
            ), array(
                'db'    => 'y_resolution',
                'dt'    => 4,
                'field' => 'y_resolution',
            ),
               array(
                'db'    => 'date_created',
                'dt'    => 5,
                'field' => 'date_created',
            ),
            // array(
            //     'db'    => 'date_modified',
            //     'dt'    => 4,
            //     'field' => 'date_modified',
            // ),
            // array(
            //     'db'    => 'row_status',
            //     'dt'    => 4,
            //     'field' => 'row_status',
            // ),
           
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname,
        );

        /* below are optional clauses only if you have more things to filter */

        /* put join clause, ex. $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id"  */

        // $joinQuery = "FROM branch_members 
        //                  JOIN member on member.idmember = branch_members.member_idmember 
        //                  JOIN branch on branch.idbranch = branch_members.branch_idbranch";

        $joinQuery ="";
        // $joinQuery = " FROM branch_members JOIN member on member.idmember = branch_members.member_idmember ";
        /* put extra where clause only if you have other specific data to find, ex. $extraWhere = "AND fname = 'John' AND lname = 'Wick'" */
        // $extraWhere = "status = 'Approved' || status = 'Denied'";
        $extraWhere = "row_status = 1 AND project_pid = ". $this->session->pid;

        /* put group clause, ex. $group_by = "age" */
        $group_by = "";


        /* this will return the data in json format */
        return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $group_by);

    }

    public function selectSharedFilesToMe()
    {   
      /* table name from your mysql database */
        $table = 'researcher';

        /* column name of primary keys from your table */
        $primaryKey = 'rid';

        /* select the columns to fetch data from your table */
        
        $columns = array(
            array(
                'db'    => 'pid',
                'dt'    => 0,
                'field' => 'pid',
            ),array(
                'db'    => 'pname',
                'dt'    => 1,
                'field' => 'pname',
            ),array(
                'db'    => 'pdesc',
                'dt'    => 2,
                'field' => 'pdesc',
            ),array(
                'db'    => 'fname',
                'dt'    => 3,
                'field' => 'fname',
            ),
            array(
                'db'    => 'mname',
                'dt'    => 4,
                'field' => 'mname',
            ),
            array(
                'db'    => 'lname',
                'dt'    => 5,
                'field' => 'lname',
            ),
            array(
                'db'    => 'sp_date_created',
                'dt'    => 6,
                'field' => 'sp_date_created',
            ),array(
                'db'    => 'grant_status',
                'dt'    => 7,
                'field' => 'grant_status',
            ),array(
                'db'    => 'rid',
                'dt'    => 8,
                'field' => 'rid',
            ),
            

        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname,
        );

        /* below are optional clauses only if you have more things to filter */

        /* put join clause, ex. 
        $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id"  */

        // $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id";

        // $joinQuery = "FROM branch_members 
        //                  JOIN member on member.idmember = branch_members.member_idmember 
        //                  JOIN branch on branch.idbranch = branch_members.branch_idbranch";

        $joinQuery ="FROM researcher 
                     JOIN share_projects ON researcher.rid = share_projects.researcher_rid
                     JOIN project ON share_projects.project_pid = project.pid
                     JOIN connections ON researcher.rid = connections.request_id
                     ";
        
        // $joinQuery = "FROM fixation_data a, fixation_file b
        // WHERE a.participant_id = b.id";
        /* put extra where clause only if you have other specific data to find, ex. $extraWhere = "AND fname = 'John' AND lname = 'Wick'" */
        // $extraWhere = "status = 'Approved' || status = 'Denied'";
        $extraWhere = "connections.status = 1 AND share_projects.to_rid =". $this->utils->decrypt_id($this->session->rid);
         //"AND connections.status = 1";

        /* put group clause, ex. $group_by = "age" */
        $group_by = "";

        /* this will return the data in json format */
        return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $group_by);

    }

    public function selectSharedFilesFromMe()
    {   
      /* table name from your mysql database */
        $table = 'researcher';

        /* column name of primary keys from your table */
        $primaryKey = 'rid';

        /* select the columns to fetch data from your table */
        
        $columns = array(
            array(
                'db'    => 'pid',
                'dt'    => 0,
                'field' => 'pid',
            ),array(
                'db'    => 'pname',
                'dt'    => 1,
                'field' => 'pname',
            ),array(
                'db'    => 'pdesc',
                'dt'    => 2,
                'field' => 'pdesc',
            ),array(
                'db'    => 'fname',
                'dt'    => 3,
                'field' => 'fname',
            ),
            array(
                'db'    => 'mname',
                'dt'    => 4,
                'field' => 'mname',
            ),
            array(
                'db'    => 'lname',
                'dt'    => 5,
                'field' => 'lname',
            ),
            array(
                'db'    => 'sp_date_created',
                'dt'    => 6,
                'field' => 'sp_date_created',
            ),array(
                'db'    => 'grant_status',
                'dt'    => 7,
                'field' => 'grant_status',
            ),array(
                'db'    => 'to_rid',
                'dt'    => 8,
                'field' => 'to_rid',
            ),
            

        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname,
        );

        /* below are optional clauses only if you have more things to filter */

        /* put join clause, ex. 
        $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id"  */

        // $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id";

        // $joinQuery = "FROM branch_members 
        //                  JOIN member on member.idmember = branch_members.member_idmember 
        //                  JOIN branch on branch.idbranch = branch_members.branch_idbranch";

        $joinQuery ="FROM researcher 
                     JOIN share_projects ON researcher.rid = share_projects.to_rid
                     JOIN project ON share_projects.project_pid = project.pid
                     JOIN connections ON researcher.rid = connections.request_id
                     ";
        
        // $joinQuery = "FROM fixation_data a, fixation_file b
        // WHERE a.participant_id = b.id";
        /* put extra where clause only if you have other specific data to find, ex. $extraWhere = "AND fname = 'John' AND lname = 'Wick'" */
        // $extraWhere = "status = 'Approved' || status = 'Denied'";
        $extraWhere = "connections.status = 1 AND share_projects.status = 1 AND share_projects.researcher_rid =". $this->utils->decrypt_id($this->session->rid);
         //"AND connections.status = 1";

        /* put group clause, ex. $group_by = "age" */
        $group_by = "";

        /* this will return the data in json format */
        return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $group_by);

    }

    public function selectConnectionList()
    {   
      /* table name from your mysql database */
        $table = 'researcher';

        /* column name of primary keys from your table */
        $primaryKey = 'rid';

        /* select the columns to fetch data from your table */
        
        $columns = array(
            array(
                'db'    => 'file_hash',
                'dt'    => 0,
                'field' => 'file_hash',
            ),array(
                'db'    => 'fname',
                'dt'    => 1,
                'field' => 'fname',
            ),array(
                'db'    => 'mname',
                'dt'    => 2,
                'field' => 'mname',
            ),array(
                'db'    => 'lname',
                'dt'    => 3,
                'field' => 'lname',
            ),
            array(
                'db'    => 'profession',
                'dt'    => 4,
                'field' => 'profession',
            ),
            array(
                'db'    => 'organization',
                'dt'    => 5,
                'field' => 'organization',
            ),
            array(
                'db'    => 'email',
                'dt'    => 6,
                'field' => 'email',
            ),array(
                'db'    => 'status',
                'dt'    => 7,
                'field' => 'status',
            ),array(
                'db'    => 'rid',
                'dt'    => 8,
                'field' => 'rid',
            ),
            

        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname,
        );

        /* below are optional clauses only if you have more things to filter */

        /* put join clause, ex. 
        $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id"  */

        // $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id";

        // $joinQuery = "FROM branch_members 
        //                  JOIN member on member.idmember = branch_members.member_idmember 
        //                  JOIN branch on branch.idbranch = branch_members.branch_idbranch";

        $joinQuery ="FROM researcher  
                    JOIN connections ON researcher.rid = connections.request_id
                    JOIN files on researcher.rid = files.researcher_rid";
        // $joinQuery = "FROM fixation_data a, fixation_file b
        // WHERE a.participant_id = b.id";
        /* put extra where clause only if you have other specific data to find, ex. $extraWhere = "AND fname = 'John' AND lname = 'Wick'" */
        // $extraWhere = "status = 'Approved' || status = 'Denied'";
        $extraWhere = "connections.researcher_rid =". $this->utils->decrypt_id($this->session->rid);
         //"AND connections.status = 1";

        /* put group clause, ex. $group_by = "age" */
        $group_by = "";

        /* this will return the data in json format */
        return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $group_by);

    }

    public function selectConnectionRequest()
    {   
      /* table name from your mysql database */
        $table = 'researcher';

        /* column name of primary keys from your table */
        $primaryKey = 'rid';

        /* select the columns to fetch data from your table */
        
        $columns = array(
            array(
                'db'    => 'file_hash',
                'dt'    => 0,
                'field' => 'file_hash',
            ),array(
                'db'    => 'fname',
                'dt'    => 1,
                'field' => 'fname',
            ),array(
                'db'    => 'mname',
                'dt'    => 2,
                'field' => 'mname',
            ),array(
                'db'    => 'lname',
                'dt'    => 3,
                'field' => 'lname',
            ),
            array(
                'db'    => 'profession',
                'dt'    => 4,
                'field' => 'profession',
            ),
            array(
                'db'    => 'organization',
                'dt'    => 5,
                'field' => 'organization',
            ),
            array(
                'db'    => 'email',
                'dt'    => 6,
                'field' => 'email',
            ),array(
                'db'    => 'rid',
                'dt'    => 7,
                'field' => 'rid',
            ),
            

        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname,
        );

        /* below are optional clauses only if you have more things to filter */

        /* put join clause, ex. 
        $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id"  */

        // $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id";

        // $joinQuery = "FROM branch_members 
        //                  JOIN member on member.idmember = branch_members.member_idmember 
        //                  JOIN branch on branch.idbranch = branch_members.branch_idbranch";

        $joinQuery ="FROM researcher 
                     JOIN connections ON researcher.rid = connections.researcher_rid
                     JOIN files on researcher.rid = files.researcher_rid";

        // $joinQuery = "FROM fixation_data a, fixation_file b
        // WHERE a.participant_id = b.id";
        /* put extra where clause only if you have other specific data to find, ex. $extraWhere = "AND fname = 'John' AND lname = 'Wick'" */
        // $extraWhere = "status = 'Approved' || status = 'Denied'";
        $extraWhere = "connections.request_id =". $this->utils->decrypt_id($this->session->rid). " AND status = 0";
         //"AND connections.status = 1";

        /* put group clause, ex. $group_by = "age" */
        $group_by = "";

        /* this will return the data in json format */
        return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $group_by);

    }

    public function selectConnectionSearch()
    {   
      /* table name from your mysql database */
        $table = 'researcher';

        /* column name of primary keys from your table */
        $primaryKey = 'rid';

        /* select the columns to fetch data from your table */
        
        $columns = array(
            array(
                'db'    => 'file_hash',
                'dt'    => 0,
                'field' => 'file_hash',
            ),array(
                'db'    => 'fname',
                'dt'    => 1,
                'field' => 'fname',
            ),array(
                'db'    => 'mname',
                'dt'    => 2,
                'field' => 'mname',
            ),array(
                'db'    => 'lname',
                'dt'    => 3,
                'field' => 'lname',
            ),
            array(
                'db'    => 'profession',
                'dt'    => 4,
                'field' => 'profession',
            ),
            array(
                'db'    => 'organization',
                'dt'    => 5,
                'field' => 'organization',
            ),
            array(
                'db'    => 'email',
                'dt'    => 6,
                'field' => 'email',
            ),array(
                'db'    => 'rid',
                'dt'    => 7,
                'field' => 'rid',
            ),
            

        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname,
        );

        /* below are optional clauses only if you have more things to filter */

        /* put join clause, ex. 
        $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id"  */

        // $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id";

        $joinQuery = "FROM researcher
                         JOIN files on researcher.rid = files.researcher_rid"; 

        // $joinQuery ="";
        // $joinQuery = "FROM fixation_data a, fixation_file b
        // WHERE a.participant_id = b.id";
        /* put extra where clause only if you have other specific data to find, ex. $extraWhere = "AND fname = 'John' AND lname = 'Wick'" */
        // $extraWhere = "status = 'Approved' || status = 'Denied'";
        $extraWhere = " researcher.rid !=". $this->utils->decrypt_id($this->session->rid);
         //"AND connections.status = 1";

        /* put group clause, ex. $group_by = "age" */
        $group_by = "";

        /* this will return the data in json format */
        return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $group_by);

    }

     public function selectParticipantFile()
    {   
      /* table name from your mysql database */
        $table = 'participant';

        /* column name of primary keys from your table */
        $primaryKey = 'ppid';

        /* select the columns to fetch data from your table */
        
        $columns = array(
            array(
                'db'    => 'ppid',
                'dt'    => 0,
                'field' => 'ppid',
            ),
            array(
                'db'    => 'date_created',
                'dt'    => 1,
                'field' => 'date_created',
            ),
            array(
                'db'    => 'name',
                'dt'    => 2,
                'field' => 'name',
            ),

        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname,
        );

        /* below are optional clauses only if you have more things to filter */

        /* put join clause, ex. 
        $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id"  */

        // $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id";

        // $joinQuery = "FROM branch_members 
        //                  JOIN member on member.idmember = branch_members.member_idmember 
        //                  JOIN branch on branch.idbranch = branch_members.branch_idbranch";

        $joinQuery ="";
        // $joinQuery = "FROM fixation_data a, fixation_file b
        // WHERE a.participant_id = b.id";
        /* put extra where clause only if you have other specific data to find, ex. $extraWhere = "AND fname = 'John' AND lname = 'Wick'" */
        // $extraWhere = "status = 'Approved' || status = 'Denied'";
        $extraWhere = "row_status = 1 ";

        /* put group clause, ex. $group_by = "age" */
        $group_by = "";

        /* this will return the data in json format */
        return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $group_by);

    }

    public function selectFixationData($id)
    {  
      /* table name from your mysql database */
        $table = 'fixation';

        /* column name of primary keys from your table */
        $primaryKey = 'fid';

        /* select the columns to fetch data from your table */
        
        $columns = array(
            array(
                'db'    => 'fid',
                'dt'    => 0,
                'field' => 'fid',
            ),
            array(
                'db'    => 'x',
                'dt'    => 1,
                'field' => 'x',
            ),
            array(
                'db'    => 'y',
                'dt'    => 2,
                'field' => 'y',
            ),
            array(
                'db'    => 'duration',
                'dt'    => 3,
                'field' => 'duration',
            )
           
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname,
        );

        /* below are optional clauses only if you have more things to filter */

        /* put join clause, ex. $joinQuery = "FROM table1 LEFT JOIN table2 ON table1.id = table2.id"  */

        // $joinQuery = "FROM branch_members 
        //                  JOIN member on member.idmember = branch_members.member_idmember 
        //                  JOIN branch on branch.idbranch = branch_members.branch_idbranch";

        $joinQuery ="";
        // $joinQuery = " FROM branch_members JOIN member on member.idmember = branch_members.member_idmember ";
        /* put extra where clause only if you have other specific data to find, ex. $extraWhere = "AND fname = 'John' AND lname = 'Wick'" */
        // $extraWhere = "status = 'Approved' || status = 'Denied'";
        // $extraWhere = "participant_ppid =". $id ;
        $extraWhere = "" ;


        /* put group clause, ex. $group_by = "age" */
        $group_by = "fid";


        /* this will return the data in json format */
        return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $group_by);


    }


     public function  deleteProject($id)
    {
        $res = array();
        $this->db->where('pid', $id);
        $this->db->delete('project');
        
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
     
    }

    public function  connectionCancelConn1($id)
    {
        $res = array();
        $this->db->where('request_id', $id);
        $this->db->where('researcher_rid',$this->utils->decrypt_id($this->session->rid));
        $this->db->delete('connections');
        
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
     
    }
    public function  connectionCancelConn2($id)
    {
        $res = array();
        $this->db->where('request_id', $this->utils->decrypt_id($this->session->rid));
        $this->db->where('researcher_rid',$id);
        $this->db->delete('connections');
        
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
     
    }

    public function  connectionReject($id)
    {
        $res = array();
        $this->db->where('request_id', $this->utils->decrypt_id($this->session->rid));
        $this->db->where('researcher_rid',$id);
        $this->db->delete('connections');
        
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
     
    }

    public function connectionRequest($data, $where)
    {
    
        $this->db->where('cid', $where);
        $this->db->update('connections', $data);
        return $this->db->affected_rows();
        
    }

    public function projectShare_grant_Update($data, $to_rid, $pid)
    {
    
        $this->db->where('researcher_rid', $this->utils->decrypt_id($this->session->rid));
        $this->db->where('to_rid', $to_rid);
        $this->db->where('project_pid', $pid);
        $this->db->update('share_projects', $data);
        return $this->db->affected_rows();
        
    }

    public function update_fixation_file($data, $where)
    {
    
        $this->db->where('ppid', $where);
        $this->db->update('participant', $data);
        return $this->db->affected_rows();
        
    }

    public function updateProject($data, $where)
    {
    
        $this->db->where('pid', $where);
        $this->db->update('project', $data);
        return $this->db->affected_rows();
        
    }



    public function updateProjectShare($data, $to_rid, $pid)
    {
    
        $this->db->where('to_rid', $to_rid);
        $this->db->where('project_pid', $pid);
        $this->db->update('share_projects', $data);
        return $this->db->affected_rows();
        
    }

    public function connectionAcceptInsert($data, $where)
    {
        $res = array();
        $this->db->insert('connections', array('status' => 1, 'request_id' => $where, 'researcher_rid' => $this->utils->decrypt_id($this->session->rid)));


        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
    }

    public function connectionAccept($data, $where)
    {

        
        $res = array();
        $this->db->where('request_id', $this->utils->decrypt_id($this->session->rid));

        $this->db->where('researcher_rid', $where);
        $this->db->update('connections', $data);
        
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
        
    }

    public function connectionCancel($data, $where)
    {

        
        $res = array();
        $this->db->where('request_id', $this->utils->decrypt_id($this->session->rid));

        $this->db->where('researcher_rid', $where);
        $this->db->update('connections', $data);
        
        $db_error = $this->db->error();
        $affected_rows = $this->db->affected_rows();

        $res = $this->output_template1($db_error, $affected_rows);
        return $res;
        
    }

    /**
     * Kent Harvey Pecorro
     */
    public function add_participant($participant = [])
    {
        if( ! empty($participant))
        {
            $data['name']          = $participant['filename'];
            $data['stimuli_sid']   = $participant['stimuli_id'];
            $data['row_status']    = 1;

            $this->db->insert('participant', $data);
            $id = $this->db->insert_id();

            return $id;
        }

        return NULL;
    }

    public function fetch_participant_data($query_type = 'get_data', $get_data = [])
    {
        $limit      = ( ! empty($get_data['length']))          ? $get_data['length']          : 0;
        $offset     = ( ! empty($get_data['start']))           ? $get_data['start']           : 0;
        $order      = ( ! empty($get_data['order']))           ? $get_data['order']           : [];
        $search_key = ( ! empty($get_data['search']['value'])) ? $get_data['search']['value'] : NULL;

        switch ($query_type)
        {
            case 'count_all':
            case 'count_filtered':
                $this->db->select('COUNT(DISTINCT(participant.ppid)) AS total ');
                break;

            case 'get_data':
                
                $this->db->select('participant.ppid AS participant_id');
                $this->db->select('participant.name');
                $this->db->select('participant.row_status');

                if ($limit > 0)
                {
                    $this->db->limit($limit);
                    $this->db->offset($offset);
                }

                if ( ! empty($order))
                {
                    foreach ($order as $column_order)
                    {
                        $this->db->order_by($get_data['columns'][$column_order['column']]['data'], $column_order['dir']);
                    }
                }
                break;

            default:
                break;
        }

        $this->db->where('participant.row_status', 1);
        if(isset($get_data['stimuli_id']) && ! empty($get_data['stimuli_id']))
        {
            $this->db->where('participant.stimuli_sid', $get_data['stimuli_id']);
        }
        if($search_key != NULL)
        {
            $this->db->group_start();
            $this->db->like('participant.stimuli_sid', $search_key);
            $this->db->or_like('participant.name', $search_key);
            $this->db->group_end();
        }

        $data = $this->db->get('participant');

        return ($query_type == 'get_data') ? $data->result_array() : $data->row_array()['total'];
    }

    public function fetch_fixation_data($query_type = 'get_data', $get_data = [])
    {
        $limit  = ( ! empty($get_data['length'])) ? $get_data['length']: 0;
        $offset = ( ! empty($get_data['start']))  ? $get_data['start'] : 0;

        switch ($query_type)
        {
            case 'count_all':
            case 'count_filtered':
                $this->db->select('COUNT(DISTINCT(fixation.fid)) AS total ');
                break;

            case 'get_data':
                
                $this->db->select('fixation.x');
                $this->db->select('fixation.y');
                $this->db->select('fixation.duration');

                if ($limit > 0)
                {
                    $this->db->limit($limit);
                    $this->db->offset($offset);
                }

                break;

            default:
                break;
        }

        $this->db->where('fixation.row_status', 1);
        $this->db->where('fixation.participant_id', $get_data['participant_id']);

        $data = $this->db->get('fixation');

        return ($query_type == 'get_data') ? $data->result_array() : $data->row_array()['total'];
    }

    public function get_aoi($stimuli_id)
    {
        $result = [];
        if($stimuli_id > 0)
        {
            $this->db->select('aoi.idaoi AS aoi_id');
            $this->db->select('aoi.x1');
            $this->db->select('aoi.y1');
            $this->db->select('aoi.x2');
            $this->db->select('aoi.y2');
            $this->db->select('aoi.x3');
            $this->db->select('aoi.y3');
            $this->db->select('aoi.x4');
            $this->db->select('aoi.y4');
            $this->db->select('aoi.stimuli_sid');

            $this->db->from('aoi');

            $this->db->where('aoi.stimuli_sid', $stimuli_id);
            $this->db->order_by('aoi_id', 'asc');
            $data   = $this->db->get();

            $result = $data->result_array();
        }

        return $result;
    }

    public function insert_analysis($analysis = [])
    {
        if( ! empty($analysis))
        {
            $data['nof']            = $analysis['nof'];
            $data['dff']            = $analysis['dff'];
            $data['tct']            = $analysis['tct'];
            $data['participant_id'] = $analysis['participant_id'];
            $data['aoi_id']         = $analysis['aoi_id'];

            $this->db->insert('fixation_analysis', $data); 
        }
    }

    public function get_participants($stimuli_id = NULL)
    {
        if($stimuli_id != NULL)
        {
            $this->db->select('participant.ppid');
            $this->db->select('participant.name');
            $this->db->where('stimuli_sid' , $stimuli_id);

            $result = $this->db->get('participant');

            return ($result->num_rows() > 0) ? $result->result_array() : [];
        }
        else
        {
            return [];
        }
    }

    public function get_nof($data = NULL)
    {
        if($data != NULL)
        {
            $participants = $data['participants_ids'];
            $aoi = $this->get_aoi($data['stimuli_id']);
            
            $nofs = [];
            foreach($aoi as $area)
            {
                $this->db->select('idaoi as aoi_id');
                $this->db->select('sum(nof) AS nof');
                $this->db->from('aoi');
                $this->db->join('fixation_analysis', 'fixation_analysis.aoi_id = aoi.idaoi', 'left');
                $this->db->where('idaoi', $area['aoi_id']);

                if( ! empty($participants))
                {
                    $this->db->group_start();
                    for($i = 0; $i < count($participants); $i++)
                    {
                        $this->db->or_where('participant_id', $participants[$i]);
                    }
                    $this->db->group_end();
                }
                else
                {
                    $this->db->where('participant_id', 0);
                }

                $data           = $this->db->get()->result_array();
                $data[0]['nof'] = (int) $data[0]['nof'];
                $nofs[]         = $data[0];
            }

            return $nofs;
            
        }
    }

    public function get_tct($data = NULL)
    {
        if($data != NULL)
        {
            $participants = $data['participants_ids'];
            $aoi = $this->get_aoi($data['stimuli_id']);
            
            $tcts = [];
            foreach($aoi as $area)
            {
                $this->db->select('idaoi as aoi_id');
                $this->db->select('sum(tct) AS tct');
                $this->db->from('aoi');
                $this->db->join('fixation_analysis', 'fixation_analysis.aoi_id = aoi.idaoi', 'left');
                $this->db->where('idaoi', $area['aoi_id']);

                if( ! empty($participants))
                {
                    $this->db->group_start();
                    for($i = 0; $i < count($participants); $i++)
                    {
                        $this->db->or_where('participant_id', $participants[$i]);
                    }
                    $this->db->group_end();
                }
                else
                {
                    $this->db->where('participant_id', 0);
                }

                $data           = $this->db->get()->result_array();
                $data[0]['tct'] = (int) $data[0]['tct'];
                $tcts[]         = $data[0];
            }

            return $tcts;
            
        }
    }

    public function get_dff($data = NULL)
    {
        if($data != NULL)
        {
            $participants = $data['participants_ids'];
            $aoi = $this->get_aoi($data['stimuli_id']);
            
            $tcts = [];
            foreach($aoi as $area)
            {
                $this->db->select('idaoi as aoi_id');
                $this->db->select('sum(dff) AS dff');
                $this->db->from('aoi');
                $this->db->join('fixation_analysis', 'fixation_analysis.aoi_id = aoi.idaoi', 'left');
                $this->db->where('idaoi', $area['aoi_id']);

                if( ! empty($participants))
                {
                    $this->db->group_start();
                    for($i = 0; $i < count($participants); $i++)
                    {
                        $this->db->or_where('participant_id', $participants[$i]);
                    }
                    $this->db->group_end();
                }
                else
                {
                    $this->db->where('participant_id', 0);
                }

                $data           = $this->db->get()->result_array();
                $data[0]['dff'] = (int) $data[0]['dff'];
                $tcts[]         = $data[0];
            }

            return $tcts;
            
        }
    }

    public function get_fixation($participant_id = NULL)
    {
        $result = [];
        if($participant_id != NULL)
        {
            $this->db->select('x');
            $this->db->select('y');
            $this->db->select('duration');

            $this->db->from('participant');
            $this->db->join('fixation', 'fixation.participant_id = participant.ppid');
            $this->db->where('participant.ppid', $participant_id);
            $this->db->where('participant.row_status', '1');
            $this->db->order_by('fid', 'asc');

            $result = $this->db->get()->result_array();   
        }

        return $result;
    }

    public function get_participant($participant_id = NULL)
    {
        $result = [];
        if($participant_id != NULL)
        {
            $this->db->select('ppid');
            $this->db->select('name');
            $this->db->where('ppid', $participant_id);
            $this->db->where('row_status', 1);

            $result = $this->db->get('participant')->row_array();
        }

        return $result;
    }

    public function remove_participant($participant_id = '')
    {
        if( ! empty($participant_id))
        {
            $participant = ['row_status' => '0'];
            $this->db->where('ppid', $participant_id);
            $this->db->update('participant', $participant);
        }
    }

}//end etdv_model

