<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/ssp.customized.class.php';
include APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Profile extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('url_helper');
		$this->load->model('etdv_model');
		$this->load->library(['session', 'encryption']);

	}

	function insert_session_post(){

	$rid = $this->post('id');
	$decrypt_id = $this->utils->decrypt_id($rid);


    $param = array(
        
          'session_time_start' => date('Y-m-d H:i:s'),
          'researcher_rid' => $decrypt_id,
          'date_created' => date('Y-m-d'),
        );  

    $id = $this->etdv_model->insert_session($param);           
    
    if($id){	

        			$this->session->set_userdata(array('logged_in'=>true, 'rid'=>$rid)); 

					$this->response(["status" => TRUE,
		            	  "message" => 'success',
		             	  "timestamp" => time(),
		             	  "samp" => $this->session->rid],
		             	  REST_Controller::HTTP_OK);
				 		
				//if current pass does not exist
		 		}else{ 		
		 			   $this->response(["status" => FALSE,
            				 "message" => $id['error'],
            				 "timestamp" => time()
            				 ],
            				 REST_Controller::HTTP_OK);
					 }

	}

	function update_profile_post(){

	
    $param = array(
        
          'fname' => $this->post('fname'),
          'mname' => $this->post('mname'),
          'lname' => $this->post('lname'),
          'profession' => $this->post('profession'),
          'organization' => $this->post('organization'),
          'email' => $this->post('email'),
          'date_modified' => date('Y-m-d'),
          
        );  

    if($this->post('npassword')){
    	$param['password'] = md5($this->post('npassword'));
    	$id = $this->etdv_model->update_profile($param, $this->post('password'));           
    }else{
    	$id = $this->etdv_model->update_profile($param, null);           
   
    }

    
    if($id){	
        			
					$this->response(["status" => TRUE,
		            	  "message" => $this->post('password'),//'success',
		             	  "timestamp" => time()],
		             	  REST_Controller::HTTP_OK);
				 		
				//if current pass does not exist
		 		}else{ 		
		 			   $this->response(["status" => FALSE,
            				 "message" => $id['error'],
            				 "timestamp" => time()
            				 ],
            				 REST_Controller::HTTP_OK);
					 }

	}

	function select_current_profile_get(){


		$res = $this->etdv_model->select_current_profile();           
    	
    	if (isset($res['error'])) {
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        } else {
        	//session logged_in = TRUE
            $this->response($res, REST_Controller::HTTP_OK);
        }
	}

	function ajax_upload_post()
    {        
        // $filename = $_FILES["image_file"]["name"];
        // echo $filename;
        // echo 'sulod ra sa ajax upload';
        // $_FILES["image_file"]["name"] = $_FILES["image_file"]["name"];

        if (isset($_FILES["image_file"]["name"])) 
        {
           
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = '*';
           
            //$config['encrypt_name'] = TRUE;
            $md5Hash = md5($_FILES["image_file"]["name"]);
            $config['file_name'] = $md5Hash;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image_file')) 
            {
               echo $this->upload->display_errors();

               $this->response([
                        'status' => false,
                        'message' => 'HTTP STATUS BAD REQUEST'

                    ], REST_Controller::HTTP_BAD_REQUEST);
            }
            else
            {
                // $data = $this->upload->data();

                  $hashf  = md5($this->upload->data('orig_name'));
                         $ext   = $this->upload->data('file_ext');
                         // echo $hashf;

                       $save = array(
                                'file_hash' => $md5Hash,
                                'file_name' => $_FILES["image_file"]["name"] ,
                                'file_version' => $this->upload->data('file_name'),

                                'file_type' => $this->upload->data('file_type'),
                                'dir_name' => $this->upload->data('full_path'),
                                'file_ext' => $this->upload->data('file_ext'),
                        );

                      $insert = $this->etdv_model->insert_profile_data($save);
                    
                       $this->response([
                                 'status' => true,
                                'message' => 'HTTP STATUS OK'

                        ], REST_Controller::HTTP_OK);

            }

        }else 
        {
            $this->response([
                        'status' => false,
                        'message' => 'HTTP STATUS BAD REQUEST'

                    ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
	

	

}

/* End of file Common.php */
/* Location: ./application/controllers/Common.php */

