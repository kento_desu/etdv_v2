<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//include APPPATH . 'libraries/ssp.customized.class.php';
//include APPPATH . 'libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class Participant extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('etdv_model');
	}

	public function get_all()
    {
		$get_data                = $this->input->get(NULL, TRUE);
		$data['data']            = $this->etdv_model->fetch_participant_data('get_data', $get_data);
		$data['recordsTotal']    = $this->etdv_model->fetch_participant_data('count_all', $get_data);
		$data['recordsFiltered'] = $this->etdv_model->fetch_participant_data('count_filtered', $get_data);

		print json_encode($data);
	}

	public function get_fixation()
	{
		$get_data                = $this->input->get(NULL, TRUE);
		$data['data']            = $this->etdv_model->fetch_fixation_data('get_data', $get_data);
		$data['recordsTotal']    = $this->etdv_model->fetch_fixation_data('count_all', $get_data);
		$data['recordsFiltered'] = $this->etdv_model->fetch_fixation_data('count_filtered', $get_data);

		print json_encode($data);
	}
	
	public function add_participant()
    {
		$dir = 'uploads/file-'.$this->session->pid.'-temp/';

		if( ! is_dir($dir))
		{
			mkdir($dir);
		}

		$targetPath = $dir.$_FILES['file_upload']['name'][0];
		move_uploaded_file($_FILES['file_upload']['tmp_name'][0], $targetPath);

		$participant = array(
								'filename'   => substr($_FILES['file_upload']['name'][0], 0, strlen($_FILES['file_upload']['name'][0]) - 4),
								'stimuli_id' => $this->session->sid
							);
		$participant_id = $this->etdv_model->add_participant($participant);

		if($participant_id != NULL)
		{
			$lines   = explode("\n", file_get_contents($targetPath));
			$headers = str_getcsv(array_shift($lines));
			$data    = array();
			foreach ( $lines as $line ) {
				$row = array();
				foreach ( str_getcsv( $line ) as $key => $field )
					$row[ $headers[ $key ] ] = $field;
				$row                   = array_filter( $row );
				$row['participant_id'] = $participant_id;
				$row['row_status']     = 1;
				if(isset($row['x']))
				{
					$data[] = $row;
				}
			}

			$this->calculate_metrics($data);
		}

		$this->etdv_model->insert_fixation($data);

		unlink($targetPath);

		print json_encode([]);
	}
	
	private function calculate_metrics($data = [])
	{
		if( ! empty($data))
		{
			$aoi = $this->etdv_model->get_aoi($this->session->sid);
			if( ! empty($aoi))
			{
				for($i = 0; $i < count($aoi); $i++)
				{
					$aoi[$i]['nof']            = 0;
					$aoi[$i]['dff']            = 0;
					$aoi[$i]['tct']            = 0;
					$aoi[$i]['participant_id'] = 0;
					$first_fixation            = 0;
					foreach($data as $row)
					{
						$aoi[$i]['participant_id'] = $row['participant_id'];
						if($row['x'] < $aoi[$i]['x4'] && $row['x'] > $aoi[$i]['x1'] && $row['y'] < $aoi[$i]['y4'] && $row['y'] > $aoi[$i]['y1'])
						{
							$aoi[$i]['nof']++;
							$aoi[$i]['tct'] += $row['duration'];
							if($first_fixation === 0 || $first_fixation === 1)
							{
								$first_fixation = 1;
								$aoi[$i]['dff'] += $row['duration'];
							}
						}
						else
						{
							if($first_fixation === 1)
							{
								$first_fixation = 2;
							}
						}
					}
					$this->etdv_model->insert_analysis($aoi[$i]);
				}
			}
		}
	}

	public function download_csv($participant_id = NULL)
	{
		if($participant_id != null)
		{
			$result = $this->etdv_model->get_fixation($participant_id);
			$participant = $this->etdv_model->get_participant($participant_id);

			$filename = (isset($participant['name'])) ? $participant['name'].'.csv' : 'download.csv';

			$file = fopen('php://output', 'w');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename="'.$filename.'";');
			fputcsv($file, ['x','y','duration'], ',');
    		foreach ($result as $line) { 
    		    fputcsv($file, $line, ','); 
			}
			fclose($file);
		}
	}

	public function remove_participant()
	{
		$participant_id = $this->input->post('participant_id', TRUE);
		$result         = 'fail';
		if( ! empty($participant_id))
		{
			$this->etdv_model->remove_participant($participant_id);
			$result = 'success';
		}

		echo $result;
	}

}


