<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/ssp.customized.class.php';
include APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Dashboard extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('url_helper');
        $this->load->model('etdv_model');
        $this->load->library(['session', 'encryption']);

    }

    public function total_projects_get()
    {
        $result = $this->etdv_model->total_projects();

        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function total_connections_get()
    {
        $result = $this->etdv_model->total_connections();

        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function total_stimuli_get()
    {
        $result = $this->etdv_model->total_stimuli();

        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function total_shared_files_get()
    {
        $result = $this->etdv_model->total_shared_files();

        $this->response($result, REST_Controller::HTTP_OK);
    }
}



/* End of file Common.php */
/* Location: ./application/controllers/Common.php */

