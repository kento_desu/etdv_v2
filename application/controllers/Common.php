<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Common extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('url_helper');
		$this->load->model('etdv_model');
		$this->load->library(['session', 'encryption']);

	}

	function insert_register_post(){
		
 		$params = array(
 
        'fname' => $this->post('fname'),
		'mname' => $this->post('mname'),
		'lname' => $this->post('lname'),
		'profession' => $this->post('profession'),
		'organization' => $this->post('organization'),
		'email' => $this->post('email'),
		'password' => md5($this->post('password')),
		'date_created' => date('Y-m-d')

         );

        $id = $this->etdv_model->insert_register($params);            

		 //if current pass exist
		 if($id){ 
					$this->response(["status" => TRUE,
		            	  "message" => 'success',
		             	  "timestamp" => time()],
		             	  REST_Controller::HTTP_OK);
				 		
				//if current pass does not exist
		 		}else{ 		
		 			   $this->response(["status" => FALSE,
            				 "message" => $id['error'],
            				 "timestamp" => time()],
            				 REST_Controller::HTTP_OK);
					 }

	}

	function login_ifexist_post(){
				
		$email = $this->post('email');
		$password = $this->post('password');

		$res = $this->etdv_model->login_ifexist($email);
		if($res['status']){
			if(md5($password) != $res['data'][0]['password']) {
				$res['status'] = false;
				// $res['password'] = $res['data'][0]['password'];
			}
		}
		unset($res['data'][0]['password']);

		// $res['id'] = $res['data'][0]['password'];

		if (isset($res['error'])) {
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        } else {
        	//session logged_in = TRUE
            $this->response($res, REST_Controller::HTTP_OK);
        }

	}

	function insert_session_post(){

	$rid = $this->post('id');
	$decrypt_id = $this->utils->decrypt_id($rid);


    $param = array(
        
          'session_time_start' => date('Y-m-d'),
          'researcher_rid' => $decrypt_id,
        );  

    $id = $this->etdv_model->insert_session($param);           
    
    if($id){	

        			$this->session->set_userdata(array('logged_in'=>true, 'rid'=>$rid, 'pid' => null, 'pname' => null, 'pdesc'=>null ,'pdate' => null, 'sid' => null, 'ppid' => null)); 

					$this->response(["status" => TRUE,
		            	  "message" => 'success',
		             	  "timestamp" => time(),
		             	  "ssid" => $this->session->rid],
		             	  REST_Controller::HTTP_OK);
				 		
				//if current pass does not exist
		 		}else{ 		
		 			   $this->response(["status" => FALSE,
            				 "message" => $id['error'],
            				 "timestamp" => time()
            				 ],
            				 REST_Controller::HTTP_OK);
					 }

	}

	function logout_get(){

		$this->session->unset_userdata(array('logged_in', 'rid', 'pid', 'pname','pdesc', 'pdate', 'sid', 'ppid'));
		$this->response(NULL, REST_Controller::HTTP_OK);
		header('location: '.base_url());

	}
	
}

/* End of file Common.php */
/* Location: ./application/controllers/Common.php */

