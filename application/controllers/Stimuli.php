<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/ssp.customized.class.php';
include APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Stimuli extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('etdv_model');

	}

	public function selectStimuliList_get()
    {
        $table = $this->etdv_model->selectStimuliList();
        $this->response($table, REST_Controller::HTTP_OK);
    }

     public function addStimulusInfo_post(){

                $params = array(
                
                'sname' => $this->post("name"),
                'sdesc' => $this->post("description"),
                'x_resolution' => $this->post('x'),
                'y_resolution' => $this->post('y'),
                'date_created' => date('Y-m-d H:i:s'),
                'row_status'   => 1,
                'project_pid' => $this->session->pid 
                );

                $id = $this->etdv_model->addStimulusInfo($params);           
                $this->session->set_userdata('stimuli_id', $id['id']);
                
                if(!$id['id']){

                        $this->response([
                                'status'=>FALSE,
                                'error'=>$id['error'],
                                'message'=>$id['message'],
                                'timestamp'=>time()
                            ], REST_Controller::HTTP_BAD_REQUEST);

                    }else{

        		$this->session->set_userdata('sid', $id['id']);

                        $this->response([
                                        'status'=>TRUE,
                                        'message'=>'added successfully',
                                        'id'=>$id['id'],
                                        'timestamp'=>time()
                                    ], REST_Controller::HTTP_OK);

                   }

    }

    public function updateStimulusInfo_post(){

                $params = array(
                
                'sname' => $this->post("name"),
                'sdesc' => $this->post("description"),
                'x_resolution' => $this->post('x'),
                'y_resolution' => $this->post('y'),
                'date_modified' => date('Y-m-d H:i:s'),
                'row_status'   => 1,
                'project_pid' => $this->session->pid 
                
                );

                $id = $this->etdv_model->updateStimulusInfo($params, $this->post("id"));           

                if(!$id){

                        $this->response([
                                'status'=>FALSE,
                                'error'=>$id['error'],
                                'message'=>$id['message'],
                                'timestamp'=>time()
                            ], REST_Controller::HTTP_BAD_REQUEST);

                    }else{

                        $this->response([
                                        'status'=>TRUE,
                                        'message'=>'success',
                                        'timestamp'=>time()
                                    ], REST_Controller::HTTP_OK);

                   }

    }

    public function addAOI_post(){

    $coordinates = $this->post("coordinates");
   
        $inc = 0;
        

        while ($inc != sizeof($coordinates)) {

                $params = array(
                

                'x1' => $coordinates[$inc][0],
                'y1' => $coordinates[$inc][1],

                'x2' => $coordinates[$inc][2],
                'y2' => $coordinates[$inc][1],

                'x3' => $coordinates[$inc][0],
                'y3' => $coordinates[$inc][3],
                
                'x4' => $coordinates[$inc][2],
                'y4' => $coordinates[$inc][3],
                                       
                'date_created'  => date('Y-m-d H:i:s'),
                'row_status'    => 1,
                'stimuli_sid'   => $this->session->sid
                                              
                );

                $inc += 1;

                //// if exist aoi id
                 $res = $this->etdv_model->aoi_ifexist($id);
                    if (isset($res['error'])) {
                        $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    } else {
                        $this->session->set_userdata('sid', $id);
                        $this->session->set_userdata('sname', $res['data'][0]['sname']);
                        $this->session->set_userdata('sdesc', $res['data'][0]['sdesc']);
                        $this->response($res, REST_Controller::HTTP_OK);
                    }

                ///////////////
                $id = $this->etdv_model->addAOI($params);           
                
                $this->session->set_userdata('aoi_id', $id['id']);

                if(!$id['id']){

                        $this->response([
                                'status'=>FALSE,
                                'error'=>$id['error'],
                                'message'=>$id['message'],
                                'timestamp'=>time()
                            ], REST_Controller::HTTP_BAD_REQUEST);

                    }else{

                        $this->response([
                                        'status'=>TRUE,
                                        'message'=>'added successfully',
                                        'member id'=>$id['id'],
                                        'timestamp'=>time()
                                    ], REST_Controller::HTTP_OK);

                   }

        } //end while

    }

    function selectParticipantFile_get(){

        $table = $this->etdv_model->selectParticipantFile();
        $this->response($table, REST_Controller::HTTP_OK);
    }

    function selectFixationData_get($id){

       
        $table = $this->etdv_model->selectFixationData($id);
        $this->response($table, REST_Controller::HTTP_OK);
    }

    function update_fixation_file_post(){

      
       $status = array(
                'row_status' => 0,
             );

       $bool = $this->etdv_model->update_fixation_file( $status, $this->post('id'));

        if($bool){
            $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
            
        }else{
             $this->response(["status" => FALSE ], REST_Controller::HTTP_BAD_REQUEST);
            
        }

    }
	
     public function updateStimulus_post($id)
        {
            $data = array(

                    'sname' => $this->post('pname'),
                    'sdesc' => $this->post('desc'), 
                    'x_resolution' => $this->post('x'), 
                    'y_resolution' => $this->post('y'), 
                    'date_modified' => date('Y-m-d H:h:s'),          
                
                );

        $bool = $this->etdv_model->updateStimulus($data, $id);
            if($bool){
                $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
            }else{
                $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
            }
        
        }

    public function deleteStimulus_post($id)
    {
        $data = array(
            'row_status' => 0          
        
        );
    
    $bool = $this->etdv_model->updateStimulus($data,$id);
        if($bool){
            $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
        }else{
            $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function deleteParticipant_File_post()
    {
            $data = array(
                'row_status' => 0          
            
            );
    
    $bool = $this->etdv_model->updateParticipantFile($data,$this->post('id'));
        if($bool){
            $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
        }else{
            $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    public function openStimuli_id_post()
    {
        $id = $this->post('id');
        $res = $this->etdv_model->stimulusId_ifexist($id);
        if (isset($res['error'])) {
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $this->session->set_userdata('sid', $id);
            $this->session->set_userdata('sname', $res['data'][0]['sname']);
            $this->session->set_userdata('sdesc', $res['data'][0]['sdesc']);
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }




}



/* End of file Common.php */
/* Location: ./application/controllers/Common.php */

