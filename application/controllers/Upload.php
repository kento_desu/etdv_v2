
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/ssp.customized.class.php';
include APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Upload extends REST_Controller {
        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
                $this->load->model('uploadDB');
                $this->load->library(['session', 'encryption']);
        }

        public function index_get()
        {
                $this->load->view('pages/upload_form');
                $this->load->view('scripts/uploadScript');
        }

        public function ajax_upload_post()
        {        

        if (isset($_FILES["image_file"]["name"])) 
        {

            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'jpg';
           
            //$config['encrypt_name'] = TRUE;
            $md5Hash = md5($_FILES["image_file"]["name"]);
            $config['file_name'] = $md5Hash;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);



            // var_dump("hello");


            if (!$this->upload->do_upload('image_file')) 
            {

               echo $this->upload->display_errors();

               $this->response([
                        'status' => false,
                        'message' => 'HTTP STATUS BAD REQUEST'

                    ], REST_Controller::HTTP_BAD_REQUEST);
            }
            else
            {
                        $save = array('row_status' => 0);
                        $update = $this->uploadDB->update($save, $this->utils->decrypt_id($this->session->rid));
                    

                        $hashf  = md5($this->upload->data('orig_name') );
                        $ext   = $this->upload->data('file_ext');

                        // $data = getimagesize($this->upload->data('full_path'));

                        $save = array(
                                'file_hash' => $md5Hash,
                                'file_name' => $_FILES["image_file"]["name"] ,
                                'file_version' => $this->upload->data('file_name'),

                                'file_type' => $this->upload->data('file_type'),
                                'dir_name' => $this->upload->data('full_path'),
                                'file_ext' => $this->upload->data('file_ext'),
                                'researcher_rid' => $this->utils->decrypt_id($this->session->rid),
                                'row_status' => 1,
                                'img_directory' => 0,
                        );

                        $insert = $this->uploadDB->insert($save);

                        
                        $this->response([
                                 'status' => true,
                                'message' => 'HTTP STATUS OK'

                        ], REST_Controller::HTTP_OK);

            }

        }else 
        {
            $this->response([
                        'status' => false,
                        'message' => 'HTTP STATUS BAD REQUEST'

                    ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function ajax_upload_stimulus_post()
        {        

        if (isset($_FILES["image_file"]["name"])) 
        {

            $config['upload_path'] = './stimuli_uploads/';
            $config['allowed_types'] = 'jpg';
           
            //$config['encrypt_name'] = TRUE;
            $md5Hash = md5($_FILES["image_file"]["name"]);
            $config['file_name'] = $md5Hash;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);


            if (!$this->upload->do_upload('image_file')) 
            {

               echo $this->upload->display_errors();

               $this->response([
                        'status' => false,
                        'message' => 'HTTP STATUS BAD REQUEST'

                    ], REST_Controller::HTTP_BAD_REQUEST);
            }
            else
            {
                        // $save = array('row_status' => 0);
                        // $update = $this->uploadDB->update($save, $this->utils->decrypt_id($this->session->rid));
                    

                        $hashf  = md5($this->upload->data('orig_name') );
                        $ext   = $this->upload->data('file_ext');

                        $save = array(
                                'file_hash' => $md5Hash,
                                'file_name' => $_FILES["image_file"]["name"] ,
                                'file_version' => $this->upload->data('file_name'),

                                'file_type' => $this->upload->data('file_type'),
                                'dir_name' => $this->upload->data('full_path'),
                                'file_ext' => $this->upload->data('file_ext'),
                                'researcher_rid' => $this->utils->decrypt_id($this->session->rid),
                                'img_directory' => 1,
                        );

                        $insert = $this->uploadDB->insert($save);

                        
                        $this->response([
                                 'status' => true,
                                'message' => 'HTTP STATUS OK'

                        ], REST_Controller::HTTP_OK);

            }

        }else 
        {
            $this->response([
                        'status' => false,
                        'message' => 'HTTP STATUS BAD REQUEST'

                    ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

           public function downloads_get()
        {
                $this->load->helper('download');

                $filename = $this->get('file_name'); // dapat hash
              
                $resbool = $this->uploadDB->ifExistFile($filename);
                $fName = $resbool[0]['file_name'];
                $fExt = $resbool[0]['file_ext'];
                $fHash = $resbool[0]['file_hash'];
                $fVer = $resbool[0]['file_version'];

                 $filePath = base_url().'uploads/'.$fVer;
                 $data = file_get_contents(base_url().'uploads/'.$fVer);
                     


                if($resbool){

                $data = file_get_contents(base_url().'uploads/'.$fVer);
                        
                        if($data != null){
         
                        $this->response([
                                 'status' => true,
                                'message' => 'HTTP STATUS OK'

                        ], REST_Controller::HTTP_OK);

                        }else{
                        $this->response([
                                'status' => false,
                                'message' => 'HTTP STATUS BAD REQUEST'

                        ], REST_Controller::HTTP_BAD_REQUEST);
                        }
               
                        
// $mime = http::get_file_mime();
// header('Pragma: public');   
// header('Expires: 0');       
// header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
// header('Cache-Control: private',false);
// header('Content-Type: '.$mime);
// header('Content-Disposition: attachment; filename="'.basename($filePath).'"');
// header('Content-Transfer-Encoding: binary');
// header('Transfer-Encoding: binary');
// header('Content-Description: File Transfer');
// header('Content-Encoding: chunked');
// header('Connection: closed');

                 force_download($fName, $data);
     
          

                }else{ 

                          $this->response([
                                'status' => false,
                                'message' => 'HTTP STATUS BAD REQUEST'

                          ], REST_Controller::HTTP_BAD_REQUEST);

                }

     }

   

}
?>