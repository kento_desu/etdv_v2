<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/ssp.customized.class.php';
include APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Shared_files extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('url_helper');
		$this->load->model('etdv_model');
		$this->load->library(['session', 'encryption']);

	}

    function selectSharedFilesToMe_get(){

        $table = $this->etdv_model->selectSharedFilesToMe();
        $this->response($table, REST_Controller::HTTP_OK);
    }

     function selectSharedFilesFromMe_get(){

        $table = $this->etdv_model->selectSharedFilesFromMe();
        $this->response($table, REST_Controller::HTTP_OK);
    }

}



/* End of file Common.php */
/* Location: ./application/controllers/Common.php */

