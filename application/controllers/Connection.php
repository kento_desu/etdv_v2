<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/ssp.customized.class.php';
include APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Connection extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('url_helper');
		$this->load->model('etdv_model');
		$this->load->library(['session', 'encryption']);

	}

    function selectConnectionList_get(){

        $table = $this->etdv_model->selectConnectionList();
        $this->response($table, REST_Controller::HTTP_OK);
    }

    function selectConnectionRequest_get(){

        $table = $this->etdv_model->selectConnectionRequest();
        $this->response($table, REST_Controller::HTTP_OK);
    }

    function selectConnectionSearch_get(){

        $table = $this->etdv_model->selectConnectionSearch();
        $this->response($table, REST_Controller::HTTP_OK);
    }

    function connection_ifexist_post(){
                
        $req_id = $this->post('data');

        $res = $this->etdv_model->connection_ifexist($req_id);
       
        if (isset($res['error'])) {
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            //session logged_in = TRUE
            $this->response($res, REST_Controller::HTTP_OK);
        }

    }

    function connectionRequest_post(){

        $cid = $this->post('data');

        $data = array(

                    'status' => 0,          
                    'request_id' => $cid,
                    'researcher_rid' =>  $this->utils->decrypt_id($this->session->rid), 
                );

        $id = $this->etdv_model->insertConnection($data);
                   
            if(!$id['id']){

                    $this->response([
                        'status'=>FALSE,
                        'error'=>$id['error'],
                        'message'=>$id['message'],
                        'timestamp'=>time()
                        ], REST_Controller::HTTP_BAD_REQUEST);

                        }else{
                                                $this->response([
                                                    'status'=>TRUE,
                                                    'message'=>'added successfully',
                                                    'member id'=>$id['id'],
                                                    'timestamp'=>time()
                                                ], REST_Controller::HTTP_OK);

                               }
    }


    public function connectionAccept_post()
        {
                $id = $this->post('id');

                $data = array(
                    'status' => 1,        
                );
        
        $bool = $this->etdv_model->connectionAccept($data,$id);
                $this->etdv_model->connectionAcceptInsert($data,$id);

            if($bool){
                $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
            }else{
                $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
            }
        }

    

    public function connectionCancel_post()
        {
    
        $id = $this->post('id');

        $bool1 = $this->etdv_model->connectionCancelConn1($id);
        $bool2 = $this->etdv_model->connectionCancelConn2($id);
                

            if($bool1){
                $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
            }else{
                $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
            }
        
            if($bool2){
                $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
            }else{
                $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
            }
        }

    public function connectionReject_post()
        {
    
        $id = $this->post('id');

        $bool = $this->etdv_model->connectionReject($id);
                

            if($bool){
                $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
            }else{
                $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
}



/* End of file Common.php */
/* Location: ./application/controllers/Common.php */

