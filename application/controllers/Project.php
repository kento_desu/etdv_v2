<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/ssp.customized.class.php';
include APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Project extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('url_helper');
		$this->load->model('etdv_model');
		$this->load->library(['session', 'encryption']);

	}

	public function projectList_get()
    {
        $table = $this->etdv_model->select_projectList();
        $this->response($table, REST_Controller::HTTP_OK);
    }

     public function connectionShare_get()
    {
        $pid = $this->get('pid');
        $result = $this->etdv_model->getConnectionShare($pid);

        $this->response($result, REST_Controller::HTTP_OK);
    }

	public function insertProject_post(){

        $params = array(
 
                'pname' => $this->post('pname'),
                'pdesc' => $this->post('desc'),
                'date_created' => date('Y-m-d H:i:s'),
                
                'researcher_rid' => $this->utils->decrypt_id($this->session->rid),
                'row_status' => 1, 
         );

        $id = $this->etdv_model->insertProject($params);            

        if(!$id['status'] ){
           
            $this->response([
                'status'=>FALSE,
                'error'=>$id['error'],
                'message'=>$id['message'],
                'timestamp'=>time()
            ], REST_Controller::HTTP_BAD_REQUEST);

            }else{

                $this->response([
                    'status'=>TRUE,
                    'message'=>'added successfully.',
                    'member id'=>$id['id'],
                    'timestamp'=>time()
                ], REST_Controller::HTTP_OK);

            }

    }

    public function updateProject_post()
        {
            $data = array(

                    'pname' => $this->post('pname'),
                    'pdesc' => $this->post('desc'), 
                    'date_modified' => date('Y-m-d H:h:s'),          
                
                );

        $bool = $this->etdv_model->updateProject($data, $this->post('no'));
            if($bool){
                $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
            }else{
                $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
            }
        
        }

        public function deleteProject_post()
	    {
	    		$data = array(

                    'row_status' => 0          
                
                );
	   
	    $bool = $this->etdv_model->updateProject($data,$this->post('no'));

	        if($bool){
	            $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
	        }else{
	            $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
	        }
	    }

    public function deleteProjectShare_post($id)
      {
          $pid  = $this->post("pid");
          $data = array(

                    'status' => 0          
                
                );
     
      $bool = $this->etdv_model->updateProjectShare($data,$id,$pid);

          if($bool){
              $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
          }else{
              $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
          }
      }

		
		public function openProject_id_post(){

		
    $id = $this->post('id');
    $status = $this->post('status');
    $name = $this->post('name');
    $date = $this->post('date');
    $shareto_id = $this->post('shareto_id');

		$res = $this->etdv_model->projectId_ifexist($id);
    // $sname = $res['data'][0]['date_created'];

		if (isset($res['error'])) {
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        } else {
        	//session logged_in = TRUE
        	$this->session->set_userdata('pid', $id);
            $this->session->set_userdata('pname', $res['data'][0]['pname']);
            $this->session->set_userdata('pdesc', $res['data'][0]['pdesc']);
            $this->session->set_userdata('pdate', $res['data'][0]['date_created']);
            $this->session->set_userdata('grant_status', $status);
            $this->session->set_userdata('shareto_name', $name);
            $this->session->set_userdata('shareto_date', $date);
            $this->session->set_userdata('shareto_id', $shareto_id);
            $this->response($res, REST_Controller::HTTP_OK);
            }

		}



        function share_connection_post(){
        
        $to_rid = (int)$this->post('to_rid');
        $grant_status = (int)$this->post('grant_status');
        $pid = (int)$this->post('pid');
        
        $params = array(
 
        'status' => 1,
        'researcher_rid' => $this->utils->decrypt_id($this->session->rid),
        'to_rid' => $to_rid,
        'project_pid' => $pid,
        'grant_status' => $grant_status,

        'sp_date_created' => date('Y-m-d H:i:s'),
        
         );

        $id = $this->etdv_model->share_connection($params);            

         //if current pass exist
         if($id){ 
                    $this->response(["status" => TRUE,
                          "message" => 'success',
                          "timestamp" => time()],
                          REST_Controller::HTTP_OK);
                        
                //if current pass does not exist
                }else{      
                       $this->response(["status" => FALSE,
                             "message" => $id['error'],
                             "timestamp" => time()],
                             REST_Controller::HTTP_OK);
                     }

        }// end share_connection function

         function updateGrant_post(){
        
        $to_rid = (int)$this->post('to_rid');
        $grant_status = (int)$this->post('grant_status');
        $pid = (int)$this->post('pid');
        
        $params = array(
        'grant_status' => $grant_status,
        );

        $id = $this->etdv_model->updateGrant($params, $to_rid, $pid);            

         //if current pass exist
         if($id){ 
                    $this->response(["status" => TRUE,
                          "message" => 'success',
                          "timestamp" => time()],
                          REST_Controller::HTTP_OK);
                        
                //if current pass does not exist
                }else{      
                       $this->response(["status" => FALSE,
                             "message" => $id['error'],
                             "timestamp" => time()],
                             REST_Controller::HTTP_OK);
                     }

        }// end share_connection function

        public function grant_ifexist_post()
        {
            $to_rid = (int)$this->post('to_rid');
            $grant_status = (int)$this->post('grant_status');
            $pid = (int)$this->post('pid');

            $result = $this->etdv_model->grant_ifexist($to_rid, $grant_status, $pid);
            $this->response($result, REST_Controller::HTTP_OK);

        }

        public function removeConnShare_post(){

            $to_rid = $this->post('to_rid');


            $id = $this->etdv_model->removeConnShare($to_rid);            

        
            if($id){ 
                    $this->response(["status" => TRUE,
                          "message" => 'success',
                          "timestamp" => time()],
                          REST_Controller::HTTP_OK);
                        
                //if current pass does not exist
                }else{      
                       $this->response(["status" => FALSE,
                             "message" => $id['error'],
                             "timestamp" => time()],
                             REST_Controller::HTTP_OK);
                     }
        }

        public function deleteShareGrant_post($id)
        {
                $data = array(

                    'row_status' => 0          
                
                );
       
        $bool = $this->etdv_model->updateStimulus($data,$id);

            if($bool){
                $this->response(["status" => TRUE], REST_Controller::HTTP_OK);
            }else{
                $this->response(["status" => FALSE], REST_Controller::HTTP_BAD_REQUEST);
            }
        }



}

/* End of file Common.php */
/* Location: ./application/controllers/Common.php */

