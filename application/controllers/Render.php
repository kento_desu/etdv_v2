<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Render extends MY_Controller {

	public function __construct(){

    	parent::__construct();
    	$this->load->model('etdv_model');
    }
  
    public function login()
    {

        if ($this->session->logged_in){
            header('location: '.base_url().'dashboard/');
        } else{
        $data['title'] = "Login";
        $this->load->view('templates/header', $data);
        $this->load->view('pages/login');
        $this->load->view('scripts/loginScript');
        $this->load->view('templates/footer');
        $this->load->view('templates/end');
        $this->load->view('templates/end');
        }
        
    }

     public function register()
    {

        if ($this->session->logged_in){
            header('location: '.base_url().'dashboard/');
        } else{
        $data['title'] = "Register";
        $this->load->view('templates/header', $data);
        $this->load->view('pages/register');
        $this->load->view('scripts/registerScript');
         $this->load->view('templates/footer');
          $this->load->view('templates/end');
        $this->load->view('templates/end');
        }
        
    }

    public function dashboard()
    {
        // $this->session->set_userdata(array('logged_in'=>true));
        if($this->session->logged_in){
        
        $data['data'] =  $this->etdv_model->get_image_data();
        $data['udata'] =  $this->etdv_model->get_user_data();
        
        $data['title']  = "Dashboard";
        $data['active'] = 'dashboard';
        $data['crequest'] = $this->etdv_model->total_request_connection();
        // $this->load->view('templates/header', $data);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('templates/sidebar',$data);
         $this->load->view('pages/dashboard');
        $this->load->view('templates/footer');
        $this->load->view('scripts/dashboardScript');
        $this->load->view('templates/end');

        }else{
            header('location: '.base_url());
        }
    }

    public function profile()
    {

        if($this->session->logged_in){

       
        $data['title']  = "Profile";
        $data['active'] = 'profile';
        $data['data'] =  $this->etdv_model->get_image_data();
        $data['data'] =  $this->etdv_model->get_image_data();
        $data['udata'] =  $this->etdv_model->get_user_data();
        $data['crequest'] = $this->etdv_model->total_request_connection();
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('pages/profile', $data);
        $this->load->view('templates/footer');
        $this->load->view('scripts/uploadScript');
        $this->load->view('scripts/profileScript');
        $this->load->view('templates/end');

        }else{
            header('location: '.base_url());
        }
    }

    public function projects()
    {

        // $this->session->set_userdata(array('logged_in'=>true));
        if($this->session->logged_in){

        $data['data'] =  $this->etdv_model->get_image_data();
        $data['udata'] =  $this->etdv_model->get_user_data();
        
        $data['title']  = "Projects";
        $data['active'] = 'projects';
        $data['crequest'] = $this->etdv_model->total_request_connection();

        $tree = array(
            
            'tree' => $this->etdv_model->getConnectionDropdown(),
            // 'tree2'=> $this->etdv_model->getConnectionShare()
         );
            

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('pages/projects', $tree);
        $this->load->view('templates/footer');
        $this->load->view('scripts/projectScript');
        $this->load->view('templates/end');

        }else{
            header('location: '.base_url());
        }
    }

    public function stimuli()
    {

        if($this->session->logged_in){

            $data['crequest'] = $this->etdv_model->total_request_connection();
            $data =  array(
                           'title' => 'Projects > Project File ',
                           'active'=> 'projects',
                           'pname' => $this->session->pname,
                           'pdesc' => $this->session->pdesc,
                           'pdate' => $this->session->pdate,
                           'data' => $this->etdv_model->get_image_data(),
                           'udata' => $this->etdv_model->get_user_data()
                        );

            $data['crequest'] = $this->etdv_model->total_request_connection();
            $this->server_side_js[] = 'scripts/stimuliScript';
            $this->server_side_js[] = 'scripts/image_mapperScript';
            $this->server_side_js[] = 'scripts/csvparserScript';

            $this->layout('pages/stimuli', $data);

        }
        else
        {
            header('location: '.base_url());
        }
    }

    public function stimuli_sharedfilepage()
    {

        if($this->session->logged_in){

            if($this->session->grant_status == 1){
                        
                       $grant_status = 'Grant Previledge:<label class="label label-success" style="width: 50%; display:block">admin</label>';
                            $stimuli_script = 'scripts/stimuliScriptenabled';
                        }else{
                          $grant_status = 'Grant Previledge:<label class="label label-warning" style="width: 50%; display:block">view-only</label>';
                            $stimuli_script = 'scripts/stimuliScriptdisabled';
                        }
                        
            $data =  array(
                           
                           'title' => 'Shared Files > Project File',
                           'active'=> 'shared_files',
                           'pid' => $this->session->pid,
                           'pname' => $this->session->pname,
                           'pname' => $this->session->pname,
                           'pdesc' => $this->session->pdesc,
                           'pdate' => $this->session->pdate,
                           'data' => $this->etdv_model->get_image_data(),
                           'udata' => $this->etdv_model->get_user_data(),
                           'status' => $grant_status,
                           'sname' => $this->session->shareto_name,
                           'sdate' => $this->session->shareto_date,
                           'file' => $this->etdv_model->get_imageto_data($this->session->shareto_id),
                           'crequest' => $this->etdv_model->total_request_connection()
                        );

             // $data['crequest'] = $this->etdv_model->total_request_connection();
            
            $this->server_side_js[] = $stimuli_script;
            $this->server_side_js[] = 'scripts/image_mapperScript';
            $this->server_side_js[] = 'scripts/csvparserScript';

            $this->layout('pages/stimuli_sharedfilepage', $data);

        }
        else
        {
            header('location: '.base_url());
        }
    }

    public function participant()
    {

        if($this->session->grant_status == 0){

            $module = 'participantdisbtn';    // admin -- disabled btn
        }else{
      
            $module = 'participant';    // admin -- enabled btn
        }

        if($this->session->logged_in){

            $data =  array(
                           'title'  => 'Project > Stimuli > Participants',
                           'active' => 'stimuli',
                           'sname'  => $this->session->sname,
                           'sdesc'  => $this->session->sdesc,
                           'data'   => $this->etdv_model->get_image_data(),
                           'udata'  => $this->etdv_model->get_user_data()
                        );

             $data['crequest'] = $this->etdv_model->total_request_connection();

            //$this->server_side_js[] = 'scripts/participantScript';
            $this->js_listeners[]   = 'Participant.property.stimuli_id = "'.$this->session->sid.'"';
            $this->js_listeners[]   = 'Participant.table.all()';
            $this->js_listeners[]   = 'Participant.table.participant_fixation()';
            $this->modules[]        = $module;

            $this->layout('pages/participants', $data);

        }
        else
        {
            header('location: '.base_url());
        }
    }

    public function add_participant_modal()
    {
        $this->load->view('modals/add-participant');
    }

    public function connection()
    {

        if($this->session->logged_in){

        
        $data =  array(
                       'title' => 'Connections',
                       'active'=> 'connection',
                       'data' => $this->etdv_model->get_image_data(),
                       'udata' => $this->etdv_model->get_user_data(),
                       'crequest' => $this->etdv_model->total_request_connection()
                        );

        $data['crequest'] = $this->etdv_model->total_request_connection();
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('pages/connection');
        $this->load->view('templates/footer');
        $this->load->view('scripts/connectionScript');
        $this->load->view('templates/end');

        }else{
            header('location: '.base_url());
        }
    }

    public function shared_files()
    {

        if($this->session->logged_in){

        
        $data =  array(
                       'title' => 'Shared Files',
                       'active'=> 'shared_files',
                       'data' => $this->etdv_model->get_image_data(),
                       'udata' => $this->etdv_model->get_user_data(),
                       'crequest' => $this->etdv_model->total_request_connection()
                       );

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('pages/shared_files');
        $this->load->view('templates/footer');
        $this->load->view('scripts/shared_filesScript');
        $this->load->view('templates/end');

        }else{
            header('location: '.base_url());
        }
    }

    public function visualize()
    {

        if($this->session->logged_in)
        {
            $data =  array(
                'title'  => 'Project > Stimuli > Visualization',
                'active' => 'stimuli',
                'pname' => $this->session->pname,
                'pdesc' => $this->session->pdesc,
                'pdate' => $this->session->pdate,
                'sname'  => $this->session->sname,
                'sdesc'  => $this->session->sdesc,
                'data'   => $this->etdv_model->get_image_data(),
                'udata'  => $this->etdv_model->get_user_data(),
                'crequest' => $this->etdv_model->total_request_connection()
            );

            $this->modules[]      = 'visualization';
            $this->js_listeners[] = 'Visualization.property.stimuli_id = "'.$this->session->sid.'"';
            $this->js_listeners[] = 'Visualization.chart.nof()';
            $this->js_listeners[] = 'Visualization.fn.filter_listener()';
            $this->js_listeners[] = 'Visualization.fn.change_category()';
            $data['participants'] = $this->_get_participants($this->session->sid);// 1 for dummy stimuli
            $this->layout('pages/visualize', $data);

        }
        else
        {
            header('location: '.base_url());
        }
    }

    private function _get_participants($stimuli_id = NULL)
    {
        $this->load->model('etdv_model');
        $result = $this->etdv_model->get_participants($stimuli_id);
        return $result;
    }

}

