<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visualization extends MY_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('etdv_model');
    }

    public function get_nof()
    {
        $get_data = $this->input->get(NULL, TRUE);
        $data['stimuli_id'] = $get_data['stimuli_id'];
        $data['participants_ids'] = isset($get_data['participants_ids']) ? $get_data['participants_ids'] : [];
        $result = $this->etdv_model->get_nof($data);

        print json_encode($result);

    }

    public function get_tct()
    {
        
        $get_data = $this->input->get(NULL, TRUE);
        $data['stimuli_id'] = $get_data['stimuli_id'];
        $data['participants_ids'] = isset($get_data['participants_ids']) ? $get_data['participants_ids'] : [];
        $result = $this->etdv_model->get_tct($data);

        print json_encode($result);

    }

    public function get_dff()
    {
        $get_data = $this->input->get(NULL, TRUE);
        $data['stimuli_id'] = $get_data['stimuli_id'];
        $data['participants_ids'] = isset($get_data['participants_ids']) ? $get_data['participants_ids'] : [];
        $result = $this->etdv_model->get_dff($data);

        print json_encode($result);

    }

    public function get_aoi()
    {
        $data = $this->etdv_model->get_aoi($this->session->sid);

        print json_encode($data);
    }
}