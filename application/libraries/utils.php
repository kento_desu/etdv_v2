<?php defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/third_party/hashids/vendor/autoload.php';

class utils
{
    protected $CI;
    private $ENCRYPTOR;
    private $CODE_GEN;

    public function __construct($params = array())
    {
        $this->CI = &get_instance();
        $this->CI->load->helper('url');
        $this->CI->config->item('base_url');
        $this->CI->load->database();
        $this->ENCRYPTOR = new Hashids\Hashids(sha1(date('Y-m-d')), 8);
        $this->CODE_GEN = new Hashids\Hashids(sha1(date('Y-m-d')), 12);
    }

    public function encrypt_id($id)
    {
        $encrypted_id = $this->ENCRYPTOR->encode($id);
        return $encrypted_id;
    }

    public function decrypt_id($id)
    {
        $decrypted_id = $this->ENCRYPTOR->decode($id);
        // return count($decrypted_id) > 0 ? $decrypted_id[0] : "";
        return isset($decrypted_id[0]) ? $decrypted_id[0] : null;
    }

    /*
    Use this when you use serializeArray in jquery.
    This will convert the serialized array to a friendly CI query builder array format as long as that the key names are the same with column names.
    $form is your input values more likely an Associative array with database column names as key names
     */
    public function unserialize_array($form)
    {
        $arr = array();
        for ($i = 0; $i < sizeof($form); $i++) {
            $arr[$form[$i]['name']] = $form[$i]['value'];
        }
        return array_filter($arr);
    }

    // convert php obj to array
    public function o2a($obj)
    {
        return json_decode(json_encode($obj), true);
    }

    /*
    $db_error passes the db error value from $this->db->error() to check if there are any errors
    $affected_rows, determine how many rows were selected or affected by the query
    $obj is the value of $query->result() containing the fetched rows of select queries
    $query is the query statement that is executed
    $debug is for debug mode, set it to true to enable debug mode
     */
    public function model_output($db_error, $affected_rows, $obj = null, $encrypt_items = array(), $query = null, $debug = true)
    {
        $res = array();
        if ($db_error['code'] == 0) {
            if ($affected_rows > 0) {
                $res['status'] = true;
                if ($debug) {
                    $res['message'] = 'Query success';
                }
                if (!empty((array) $obj)) {
                    $res['data'] = $this->o2a($obj);
                    if (!empty($encrypt_items)) {
                        for ($i = 0; $i < sizeof($res['data']); $i++) {
                            for ($j = 0; $j < sizeof($encrypt_items); $j++) {
                                $res['data'][$i][$encrypt_items[$j]] = $this->encrypt_id($res['data'][$i][$encrypt_items[$j]]);
                            }
                        }
                    }
                    if ($debug) {
                        $res['total_rows'] = $affected_rows;
                        $res['message']    = 'Query success with data';
                    }
                }
            } else {
                $res['status'] = false;
                if ($debug) {
                    $res['message'] = 'Query success but there were no rows were affected or to be fetched';
                }
            }
        } else {
            $res['status'] = false;
            $res['error']  = 'Enable debug mode to see the error';
            if ($debug) {
                $res['code']  = $db_error['code'];
                $res['error'] = $db_error['message'];
            }
            $res['message'] = 'Connection or database problem';
        }

        if ($debug && $query) {
            $res['query'] = trim($query);
        }
        return $res;
    }

    public function multiple_output_checker($res)
    {
        $result['status'] = true;
        $success          = 0;
        $fail             = 0;
        for ($i = 0; $i < sizeof($res); $i++) {
            if ($res[$i]['status']) {
                $success += 1;
            } else {
                $fail += 1;
            }
        }
        $result['executions']['success'] = $success;
        $result['executions']['fail']    = $fail;
        $result['executions']['data']    = $res;
        return $result;
    }

    /*
    This function is useful when querying with multiple or joined tables.
    When $form contains columns from different tables, this will filter out or remove the columns that are note within the table
    $form is your input values more likely an Associative array with database column names as key names
    $table is your table name
     */
    public function model_col_check($table, $form, $decrypt = array())
    {
        $params = array();
        foreach ($form as $field_name => $field_value) {
            if ($this->CI->db->field_exists($field_name, $table)) {
                if($field_value != ""){
                    $params[$field_name] = $field_value;
                }
            }
        }

        if (!empty($decrypt)) {
            foreach ($params as $field_name => $field_value) {
                for ($i = 0; $i < sizeof($decrypt); $i++) {
                    if ($decrypt[$i] == $field_name) {
                        $field_value = $this->decrypt_id($field_value);
                    }
                    $params[$field_name] = $field_value;
                }
            }
        }
        return $params;
    }

    public function model_key_options($table, $show_keys = true)
    {
        $params = array();
        $sql    = "SHOW COLUMNS FROM {$table}";
        $query  = $this->CI->db->query($sql);
        foreach ($query->result() as $row) {
            if (!$show_keys && ($row->Key == 'PRI' || $row->Key == 'MUL')) {
                continue;
            } else {
                $params[] = $table . '.' . $row->Field;
            }
        }
        return $params;
    }

    /*
    This function is for making passwords.
    Use php native function password_verify(RAW_PASSWORD, ENCRYPTED_PASSWORD) to verify passwords.
     */
    public function password_encoder($password)
    {
        $options = [
            'cost' => 8,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    public function random_code($size)
    {
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $code             = '';
        for ($i = 0; $i < $size; $i++) {
            $code .= $characters[rand(0, $charactersLength - 1)];
        }
        return $code;
    }

    public function datatables($columns, $table, $request, $join = array(), $where = array())
    {
        $this->CI->db->select('SQL_CALC_FOUND_ROWS '.implode(", ",$columns), false);
        $this->CI->db->from($table);
        if(!empty($join)){
            for ($i=0; $i < sizeof($join); $i++) { 
                $this->CI->db->join($join[$i][0], $join[$i][1], $join[$i][2]);
            }
        }

        if(isset($request['search']) && $request['search']['value'] != ""){
            $this->CI->db->group_start();
            for ($i=0; $i < sizeof($columns); $i++) { 
                $this->CI->db->or_like($columns[$i], $request['search']['value']);
            }
            $this->CI->db->group_end();
        }

        if(!empty($where)){
            for ($i=0; $i < sizeof($join); $i++) { 
                if($join[$i][2] == 'or' || $join[$i][2] == 'OR'){
                    $this->CI->db->or_where($where[$i][0], $where[$i][1]);
                }else{
                    $this->CI->db->where($where[$i][0], $where[$i][1]);
                }
            }
        }

        if(isset($request['order'])){
            $this->CI->db->order_by($columns[$request['order'][0]['column']], $request['order'][0]['dir']);
        }

        if(isset($request['length']) && isset($request['start']) && $request['length'] != -1){
            $this->CI->db->limit((int)$request['length'], (int)$request['start']);
        }

        $query = $this->CI->db->get();
        $q_string = $this->CI->db->last_query();

        $result_set = array();
        $r = $this->o2a($query->result());
        for ($i=0; $i < sizeof($r); $i++) { 
            $result = array();
            for ($j=0; $j < sizeof($columns); $j++) { 
                $result[] = $r[$i][str_replace('`', '', $columns[$j])];
            }
            $result_set[] = $result;
        }

        $recordsFiltered = (int)$query->num_rows();
        $recordsTotal = (int)$query->num_rows();
        if(isset($request['length'])){
            $query = $this->CI->db->query('SELECT FOUND_ROWS() AS `Count`');
            $recordsTotal = (int)$query->row()->Count;
        }

        return ['data'=>$result_set, 'draw'=>$request['draw'], 'query'=>$q_string, 'recordsTotal'=>$recordsTotal, 'recordsFiltered'=>$recordsFiltered];
    }
}
