 <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    <a style="margin-left: 260px;" class="navbar-brand" href="#"><?php echo $title;  ?> </a>
                    </div>
                    <div class="collapse navbar-collapse">

                        <ul class="nav navbar-nav navbar-right">
                           
                            <li>
                                <a href="<?= base_url();?>connection-request">
                                    <!-- <i class="material-icons">bubble_chart</i> -->
                                    <span class="notification" style="width: 150px; height:20px ; margin-top: 9px; "><i class="material-icons">bubble_chart</i><?php echo $crequest['data_fetch'][0]['conn']; ?> Connection Requests</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url();?>logout">
                                    <i class="material-icons">person</i>logout
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                            <li></li>
                        </ul>
                       <!--  <form class="navbar-form navbar-right" role="search">
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form> -->
                    </div>
                </div>
            </nav>