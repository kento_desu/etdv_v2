
        </div>
    </div>

</body>

<script>
    var base_url = "<?php echo base_url();?>";
</script>

<!-- add row -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


<!-- form modal -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



<!--   Core JS Files   -->
<script src="<?= base_url();?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url();?>assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="<?= base_url();?>assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="<?= base_url();?>assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="<?= base_url();?>assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= base_url();?>assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?= base_url();?>assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= base_url();?>assets/js/demo.js"></script>

<!-- js for datatables -->
 	<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url();?>assets/js/altEditor/dataTables.altEditor.free.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.1.2/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.0.2/js/dataTables.responsive.min.js"></script>

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    
    <!-- select 2 -->
   
<!-- bootstrap fileinput-->
<script src="<?php echo base_url('vendor/kartik-v/bootstrap-fileinput/js/plugins/piexif.min.js');?>" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
    This must be loaded before fileinput.min.js -->
<script src="<?php echo base_url('vendor/kartik-v/bootstrap-fileinput/js/plugins/sortable.min.js');?>" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for 
    HTML files. This must be loaded before fileinput.min.js -->
<script src="<?php echo base_url('vendor/kartik-v/bootstrap-fileinput/js/plugins/purify.min.js');?>" type="text/javascript"></script>
<!-- the main fileinput plugin file -->
<script src="<?php echo base_url('vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js');?>"></script>
<!-- papaparse -->

<script src="<?php echo base_url('vendor/sweetalert2/js/sweetalert2.all.js');?>"></script>
<script src="<?php echo base_url('node_modules/jspdf/dist/jspdf.min.js');?>"></script>

<script src="<?= base_url();?>assets/js/papaparse.js"></script>
<script src="<?= base_url();?>assets/js/player.js"></script>

<script src="<?php echo base_url('assets/js/helper.js');?>"></script>

<script src="<?= base_url();?>assets/js/Chart.bundle.min.js"></script>

<!--   JS plugins -->
<?php if(isset($js_plugins)): ?>
    <?php foreach ($js_plugins as $plugin) : ?>
        <script src="<?php echo base_url('assets/js/plugins').'/'.$plugin.'.js';?>"></script>
    <?php endforeach; ?>
<?php endif;?>

<!--   JS Modules -->
<?php if(isset($modules)): ?>
    <?php foreach ($modules as $module) : ?>
        <script src="<?php echo base_url('assets/js/modules').'/'.$module.'-module.js';?>"></script>
    <?php endforeach; ?>
<?php endif;?>

<!--   Custom JS-->
<?php if(isset($custom_js)): ?>
    <?php foreach($custom_js as $js) : ?>
        <script src="<?php echo base_url('assets/js').'/'.$js.'.js';?>"></script>
    <?php endforeach; ?>
<?php endif;?>

<script type="text/javascript">
    $(document).ready(function () {
		<?php foreach ($js_listeners as $listeners): ?>
			<?php echo $listeners; ?>;
		<?php endforeach;?>
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>