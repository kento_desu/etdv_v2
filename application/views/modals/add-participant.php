<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Add Participant/s</h4>
</div>
<div class="modal-body">
    <div class="file-loading">
        <input id="file-upload" name="file_upload[]" multiple type="file">
    </div>
</div>