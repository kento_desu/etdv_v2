<script type="text/javascript">


$(function(){
    $('#register-form').submit(function (event){
        event.preventDefault();
        var dataString = $(this).serialize();

        var pass = $('[name="password"]').val();
        var cpass = $('[name="cpassword"]').val();
        
        if(pass != cpass){
            alert('Password not matched!');
            $('.password').addClass('has-error');
            $('[name="password"]').val('');
            $('[name="cpassword"]').val('');
        }
        else{

            $('#loader').html('<div class="lds-facebook"><div></div><div></div><div></div></div>');

             $.ajax({
            data: dataString,
            dataType: 'json',
            type: 'post',
            url: '<?= base_url();?>insert_register', 
            //cache: false, // optional
            error: function (err) {
                alert('error');
            },

            success: function (msg) {
                
                window.location = '<?= base_url();?>login';
                alert('Registered successfully!');
            }
         });

        }


    });

})

</script>