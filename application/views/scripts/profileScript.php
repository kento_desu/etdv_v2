<script type="text/javascript">


$(function(){

	//get current profile
	load_profile();

	function load_profile(){

		$.ajax({
            // data: dataString,
            dataType: 'json',
            type: 'get',
            url: '<?= base_url();?>get-profile', 
            //cache: false, // optional
            error: function (err) {
                alert('error');
            },

            success: function (msg) {
               $('.profile').addClass('is-focused');
               $('.profile').removeClass('is-empty');
               // alert(msg.data[0]['password']);
               $('[name="fname"]').val(msg.data[0]['fname']);
               $('[name="mname"]').val(msg.data[0]['mname']);
               $('[name="lname"]').val(msg.data[0]['lname']);
               $('[name="profession"]').val(msg.data[0]['profession']);
               $('[name="organization"]').val(msg.data[0]['organization']);
               $('[name="email"]').val(msg.data[0]['email']);

            }
         });

	}	

    

    $('#profile-form').submit(function (event){
       event.preventDefault();
       var dataString = $(this).serialize();

       // alert(dataString);

    if( $('[name="password"]').val() ==  $('[name="cpassword"]').val()){
      	
      	 $.ajax({
            data: dataString,
            dataType: 'json',
            type: 'post',
            url: '<?= base_url();?>update-profile', 
            //cache: false, // optional
            error: function (err) {
                alert('error');
            },

            success: function (msg) {
                
                if(msg.status){
                	alert('Updated');
                load_profile();

                $('[name="npassword"]').val('');
                $('[name="password"]').val('');
                $('[name="cpassword"]').val('');


            }else{

            	if($('[name="password"]').val() == '' && $('[name="cpassword"]').val() == '' ){
            		alert('Up-to-date already');
            	}else{
            		alert('Invalid password');
            	}

            	 load_profile();

                // window.location = '<?= base_url();?>login';
            }
                 
            }
         });
      
      }else{

      	alert('Invalid Password Combination');

        // $('#password').addClass('has-error');
        $('[name="password"]').val('');
        $('[name="cpassword"]').val('');
      }


    });

})

</script>