<script type="text/javascript">

var connTableSearch = [{
          title: "#"
        },{
          title: "Project Name"
        },{
          title: "Project Description"
        },{
          title: ""
        },{
          title: ""
        },{
          title: "Shared By"
        },{
          title: "Date Shared"
        },{
          title: '<center>grant_status</center>'
        },{
          title: ''
        },
        ];

  var name;

  search = $('#filesTo').DataTable({
          columns: connTableSearch,
          dom: 'Bfrtip',        // Needs button container
          select: 'single',
          responsive: true,
          // altEditor: true,     // Enable altEditor
          processing: true,
          "pageLength": 8,
          serverSide: true,
          keys: true,
          "searching": true,
           "bInfo": false,
          ajax:{
                url: "<?= base_url();?>Shared_files/selectSharedFilesToMe",
                type: "get"                            
               }, 

          columnDefs: [
          			 {
                        
                        targets: 8,
                        visible: false
                        // render: function (data) { return data+"  hi"; }
          },{
                        
                        targets: 0,
                        visible: false
                        // render: function (data) { return data+"  hi"; }
          },{
						targets: 1,
                       
						render: function (data){

							name = data;
							return data;
						}
					},{
						targets: 2,
                        
						render: function (data){
							name = name +' '+data;
							return data;
						}
					},{
            targets: 3,
                        visible: false, 

            render: function (data){
              name = data;
              return data;
            }
          },{
            targets: 4,
                        visible: false, 

            render: function (data){
              name = name + ' '+ data;
              return data;
            }
          },{
            targets: 5,
                        // visible: false, 

            render: function (data){
              name = name + ' '+ data;
              return name;
            }
          },
          				{
                        
                        targets: 7,
                        // visible: false
                        render: function (data) { 
                        	
                          // onclick="addConnection('+data+');"
                        	if(data == 1){
                        
                        data = '<center><span class="label label-success" style="width: 70%; display:block">admin</span></center>';
                            
                        }else{
                          data = '<center><span class="label label-warning" style="width: 70%; display:block">view-only</span></center>';
                          
                        }
                        	return data; 

                        }

                    
                      }],


          buttons: [ {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">folder_open</i> Open Project File',
            name: 'open',        // do not change name
            action: function ( e, dt, button, config ) {

                var id = parseInt(dt.row( { selected: true } ).data()[0]);
                // open_project(id,'','','','','', '');


                var fname = dt.row( { selected: true } ).data()[3];
                var mname = dt.row( { selected: true } ).data()[4];
                var lname = dt.row( { selected: true } ).data()[5];
                var date = dt.row( { selected: true } ).data()[6];
                var status = parseInt(dt.row( { selected: true } ).data()[7]);
                var shareto_id = parseInt(dt.row( { selected: true } ).data()[8]);
                // open_project(id,status,date,fname,mname,lname, shareto_id);
                // alert(id);
                // alert("shared files from");
                open_project(id,status,date,fname,mname,lname,shareto_id);

                // alert(id);
               
                
                }
          },
        ]

        }); // end conn search


var connTableReq = [
          {
          title: "#"
        },{
          title: "Project Name"
        },{
          title: "Project Description"
        },{
          title: ""
        },{
          title: ""
        },{
          title: "Shared To"
        },{
          title: "Date Shared"
        },{
          title: '<center>grant_status</center>'
        },{
          title: ''
        },

        ];



  filesFrom = $('#filesFrom').DataTable({
          columns: connTableReq,
          dom: 'Bfrtip',        // Needs button container
          select: 'single',
          responsive: true,
          // altEditor: true,     // Enable altEditor
          processing: true,
          "pageLength": 8,
          serverSide: true,
          keys: true,
          "searching": true,
           "bInfo": false,
          ajax:{
                url: "<?= base_url();?>Shared_files/selectSharedFilesFromMe",
                type: "get"                            
               }, 

          columnDefs: [
          {
                        
                        targets: 8,
                        visible: false
                        // render: function (data) { return data+"  hi"; }
          },
                  {
                        
                        targets: 0,
                        visible: false
                        // render: function (data) { return data+"  hi"; }
          },{
            targets: 1,
                       
            render: function (data){

              name = data;
              return data;
            }
          },{
            targets: 2,
                        
            render: function (data){
              name = name +' '+data;
              return data;
            }
          },{
            targets: 3,
                        visible: false, 

            render: function (data){
              name = data;
              return data;
            }
          },{
            targets: 4,
                        visible: false, 

            render: function (data){
              name = name + ' '+ data;
              return data;
            }
          },{
            targets: 5,
                        // visible: false, 

            render: function (data){
              name = name + ' '+ data;
              return name;
            }
          },
                  {
                        
                        targets: 7,
                        // visible: false
                        render: function (data) { 
                          
                          // onclick="addConnection('+data+');"
                          if(data == 1){
                        
                        data = '<center><span class="label label-success" style="width: 70%; display:block">admin</span></center>';
                            
                        }else{
                          data = '<center><span class="label label-warning" style="width: 70%; display:block">view-only</span></center>';
                          
                        }
                          return data; 

                        }

                    
                      }],


          buttons: [ {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">folder_open</i> Open Project File',
            name: 'open',        // do not change name
            action: function ( e, dt, button, config ) {

                var id = parseInt(dt.row( { selected: true } ).data()[0]);
                var fname = dt.row( { selected: true } ).data()[3];
                var mname = dt.row( { selected: true } ).data()[4];
                var lname = dt.row( { selected: true } ).data()[5];
                var date = dt.row( { selected: true } ).data()[6];
                var status = parseInt(dt.row( { selected: true } ).data()[7]);
                var shareto_id = parseInt(dt.row( { selected: true } ).data()[8]);

                open_project(id,status,date,fname,mname,lname, shareto_id);
                
               
                
                }
          },
        
        ]

        }); // end conn1



function open_project(id,status,date,fname,mname,lname, shareto_id){

    // alert(id+status+fname+man+date+shareto_id);
    var name = fname+' '+mname+' '+lname;
      $.ajax({
            url: "<?= base_url();?>Project/openProject_id",
            data: {id:id, status:status, name:name,  date:date, shareto_id:shareto_id},
            dataType: 'json',
            type: 'post', 
            
            success: function(msg) {
               
              // alert('success');
              window.location = '<?= base_url();?>stimuli_sharedfilepage';

            },
            error: function(jqXHR, status, error) {
                
              alert('error');
            
            }
        });

    }

  // function open_projectto(id){

  
  //     $.ajax({
  //           url: "<?= base_url();?>Project/openProjectto_id",
  //           data: {id:id},
  //           dataType: 'json',
  //           type: 'post', 
            
  //           success: function(msg) {
               
  //             // alert('success');
  //             window.location = '<?= base_url();?>stimuli_sharedfilepage';

  //           },
  //           error: function(jqXHR, status, error) {
                
  //             alert('error');
            
  //           }
  //       });

  //   }


</script>