<script type="text/javascript">
	
$(document).ready(function() {

// projects
$.ajax(
        {
            type: 'GET',
            url: '<?= base_url();?>dashboard/total_projects',
            dataType: 'json',
            success: function(data){

                $('#total_projects').html(data['data_fetch'][0]['projects']);

                setTimeout( function() {
                    getVouchers();
                }, 10000 );
            }
        });

//connections
$.ajax(
        {
            type: 'GET',
            url: '<?= base_url();?>dashboard/total_connections',
            dataType: 'json',
            success: function(data){

                $('#total_connections').html(data['data_fetch'][0]['connections']);

                setTimeout( function() {
                    getVouchers();
                }, 10000 );
            }
        });

//stimuli uploads
$.ajax(
        {
            type: 'GET',
            url: '<?= base_url();?>dashboard/total_stimuli',
            dataType: 'json',
            success: function(data){

                $('#total_stimuli').html(data['data_fetch'][0]['stimuli']);

                setTimeout( function() {
                    getVouchers();
                }, 10000 );
            }
        });

//share file
$.ajax(
        {
            type: 'GET',
            url: '<?= base_url();?>dashboard/total_shared_files',
            dataType: 'json',
            success: function(data){

                $('#total_shared_files').html(data['data_fetch'][0]['shared_files']);

                setTimeout( function() {
                    getVouchers();
                }, 10000 );
            }
        });




}); // end document



</script>