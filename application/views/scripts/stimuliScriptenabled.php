
<script src="<?php echo base_url('vendor/kartik-v/bootstrap-fileinput/js/').'fileinput.min.js';?>"></script>

<script>
        $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-success').trigger('click');
});
      // 
    
    function saveStimulusInfo(){


      var id =  $( "input[name='no']" ).val();
      var name =  $( "input[name='sname']" ).val();
      var description =  $( "input[name='sdesc']" ).val();
      var x =  $( "input[name='x']" ).val();
      var y =  $( "input[name='y']" ).val();


      if($('#btnOpt').val() == 0){
            if(name != "" && description != ""){
            // alert('inside');

             $.ajax({
                data: {id:id,description:description, name: name, x:x, y:y},
                dataType: 'json',
                type: 'post',
                url: '<?= base_url();?>stimuli/addStimulusInfo', 
                error: function (err) {
                    alert('error');
                },

                success: function (msg) {
                    s_table.ajax.reload();
                    alert('success');
                  }
             });

          }else{
            alert('Please complete form requirements');
          }

      }else{
        if(name != "" && description != ""){
        // alert('inside');

         $.ajax({
            data: {id:id,description:description, name: name, x:x, y:y},
            dataType: 'json',
            type: 'post',
            url: '<?= base_url();?>stimuli/updateStimulusInfo', 
            error: function (err) {
                alert('error');
            },

            success: function (msg) {
                
                s_table.ajax.reload();
                alert('success');
              }
         });

      }else{
        alert('Please complete form requirements');
      }
      }


    }

    function updateStimulusInfo(){


      var id =  $( "input[name='no']" ).val();
      var name =  $( "input[name='sname']" ).val();
      var description =  $( "input[name='sdesc']" ).val();
      var x =  $( "input[name='x']" ).val();
      var y =  $( "input[name='y']" ).val();

      // alert('hi');

      if(name != "" && description != ""){
        // alert('inside');

         $.ajax({
            data: {id:id,description:description, name: name, x:x, y:y},
            dataType: 'json',
            type: 'post',
            url: '<?= base_url();?>stimuli/updateStimulusInfo', 
            error: function (err) {
                alert('error');
            },

            success: function (msg) {
                
                alert('success');
              }
         });

      }else{
        alert('Please complete form requirements');
      }
      

      // }

    }

      $(document).ready(function() {

        var dataSet = [
          ["02-17-18", "project Layout","02-19-18"],
          ["0228", "project Layout",  "02-19-18"],
          ["111", "project Layout","02-19-18"],
        
        ];

       

        
        var columnDefs = [{
          title: "Date"
        }, {
          title: "Project Name"
        },   {
          title: "Last Update"
        }];
    

        var columnDefs = [{
          title: "#"
        }, {
          title: "Stimuli Name"
        }, {
          title: "Description"
        }, {
          title: "x"
        },{
          title: "Resolution"
        },{
          title: "Date Created"
        },

        ];


        var res;
        // var s_table;


        s_table = $('#s_table').DataTable({
          "sPaginationType": "full_numbers",
          columns: columnDefs,
          dom: 'Bfrtip',        // Needs button container
          select: 'single',
          responsive: true,
          processing: true,
          serverSide: true,
          keys: true,
          ajax:{
                url: "<?= base_url();?>Stimuli/selectStimuliList",
                type: "get"                            
               }, 

          columnDefs: [{
                        
                       targets: 0,
                        visible: false,
                       
                        },{
                        
                       targets: 3,
                        visible: false,
                        render: function (data) { 
                          res = data;
                          return data; 
                        }
                        },

                        {
                        targets: 4,
                        render: function (data) { 
                          return res+' x '+data; 
                        }
                        },

                      ],

          buttons: [
          
          {
            text: '<i class="material-icons">add_circle_outline</i>Add New Stimuli',
            name: 'addstim',
             action: function ( e, dt, button, config ) {   



             $('#form-id')[0].reset();    

             $('#saveBtn').html('<button type="button" class="btn btn-default pill-left" data-dismiss="modal">Cancel</button><button onclick="saveStimulusInfo()" class="btn btn-primary nextBtn pull-right" type="button">Save and Proceed</button>');   

              $('#stimuliModal').modal({
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                });

              $('#btnOpt').val('0');
              
              }

            },{
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">grain</i>View Participant-Fixation Data',
            name: 'open',        // do not change name

            action: function ( e, dt, button, config ) {
                  var id = parseInt(dt.row( { selected: true } ).data()[0]);
                  
                  open_stimuli(id,);
                  // alert('hello');
                }
          },

          {
            text: '<i class="material-icons">border_color</i> Edit Stimuli',
            name: 'edit',        // do not change name
            extend: 'selected',
            action: function ( e, dt, button, config ) {
                  
                  $('#saveBtn').html('<button type="button" class="btn btn-default pill-left" data-dismiss="modal">Cancel</button><button onclick="updateStimulusInfo()" class="btn btn-primary nextBtn pull-right" type="button">Update and Proceed</button>');

                  $('.modal-title').html('Edit Stimulus Info');
                  
                  $('input[name="opt"]').val('edit');
                  $('input[name="no"]').val(dt.row( { selected: true } ).data()[0]);
                  $('input[name="sname"]').val(dt.row( { selected: true } ).data()[1]);
                  $('input[name="sdesc"]').val(dt.row( { selected: true } ).data()[2]);
                  $('input[name="x"]').val(dt.row( { selected: true } ).data()[3]);
                  $('input[name="y"]').val(dt.row( { selected: true } ).data()[4]);
                   $('#stimuliModal').modal({
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                });

              $('#btnOpt').val('1');
              // alert($('#btnOpt').val());

                  id(dt.row( { selected: true } ).data()[0]);

                  }
          },

          {
            extend: 'selected',
            text: '<i class="material-icons">delete_forever</i> Delete',
            name: 'delete',      // do not change name
            
            enabled: false,
            action: function ( e, dt, button, config ) {
                  
                  
                $('input[name="opt"]').val('delete');
                $('input[name="no"]').val(dt.row( { selected: true } ).data()[0]);
                $('input[name="pname"]').val(dt.row( { selected: true } ).data()[1]);
                $('input[name="desc"]').val(dt.row( { selected: true } ).data()[2]);
                $('input[name="x"]').val(dt.row( { selected: true } ).data()[3]);
                $('input[name="y"]').val(dt.row( { selected: true } ).data()[4]);
                  
                
                
                $('#submitForm').html('Delete');
                
                $('.modal-title').html('Are you sure deleting this project?');
                
                $('#editor-modal').modal('show');    

               
                }
		 },
		 {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">show_chart</i>View Visualization',
            name: 'open',        // do not change name
            action: function ( e, dt, button, config ) {
                  var id = parseInt(dt.row( { selected: true } ).data()[0]);
                  open_visualization(id);
                }
          }
        
          ]

        });       

        var columnDefs = [{
          title: "Date"
        }, {
          title: "Project Name"
        },   {
          title: "Last Update"
        }];
      }); // end docu ready


      function editStimuli(){

       		$('#editStimuliModal').modal({
                    show: true,
                    // keyboard: false,
                    // backdrop: 'static'
                });      
      }

      $("#form").on("submit", function(e) {
      e.preventDefault();

        var url;
        var dataString = $(this).serialize();
      
        if($('input[name="opt"]').val() == 'edit'){

            var id = $('input[name="no"]').val();
            url = "<?php echo site_url('Stimuli/updateStimulus')?>/" + id;
        }else{

            var id = $('input[name="no"]').val();
            url = "<?php echo site_url('Stimuli/deleteStimulus')?>/" + id; 
        }

        $.ajax({
            url: url,
            data: dataString,
            dataType: 'json',
            type: 'post', 
            
            success: function(msg) {
                if(msg.status){
                  $('#editor-modal').modal('hide'); 
                  s_table.ajax.reload();
                
                
                }else{
                  alert('else');
                }
            },
            error: function(jqXHR, status, error) {
                console.log(status + ": " + error);
                alert('error');
            }
        });
        
    });
     
    $("#submitForm").on('click', function() {
        $("#form").submit();
	});
	
	function open_visualization(id) {
		$.ajax({
            url     : "<?=base_url();?>Stimuli/openStimuli_id",
            data    : {id : id},
            dataType: 'json',
            type    : 'post', 
            
            success: function(msg) {
            
              	window.location = '<?= base_url();?>visualization/';

            },
            error: function(jqXHR, status, error) {
                
              alert('error');
            
            }
		});
	}

function open_stimuli(id){
      	$.ajax({
            url     : "<?=base_url();?>Stimuli/openStimuli_id",
            data    : {id : id},
            dataType: 'json',
            type    : 'post', 
            
            success: function(msg) {
            
              	window.location = '<?= base_url();?>participants/';

            },
            error: function(jqXHR, status, error) {
                
              alert('error');
            
            }
        });

    }





    </script>