<script type="text/javascript">


$(function(){

    $('#login-form').submit(function (event){
        event.preventDefault();
        var dataString = $(this).serialize();

        // alert(dataString);

        $('#loader').html('<div class="lds-facebook"><div></div><div></div><div></div></div>');

         $.ajax({
            data: dataString,
            dataType: 'json',
            type: 'post',
            url: '<?= base_url();?>login_ifexist', 
            //cache: false, // optional
            error: function (err) {
                alert('error');
            },

            success: function (msg) {
                if(msg.status){

                        $.ajax({
                        data: {id: msg.data[0]['rid']},
                        dataType: 'json',
                        type: 'post',
                        url: '<?= base_url();?>session', 
                        //cache: false, // optional
                        error: function (err) {
                            alert('error');


                        },

                        success: function (msg) {
                           // alert(msg.samp);
                            window.location = '<?= base_url();?>dashboard';
                               
                        }
                     });

                }else{
                    $('#login-form')[0].reset();
                    alert('Invalid email/password');
                    $('#reloader').html('<button type="submit"  class="btn btn-def btn-block"><div id="loader">Login</div></button>');
                }
            }
         });


    });

})

</script>