<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<script type="text/javascript">
   
    $(document).ready(function() {
        $('#upload_form').submit(function(e) {
            e.preventDefault();

            if ($('#image_file').val() == '') {
                alert('please select file');
            } else {
                //alert('upload');
                var data = new FormData(this);
                
                upload(data);

            }

        });
    
    });


    function upload(data) {

        $.ajax({                
            url: '<?= base_url();?>upload-file',
                            type: "POST",
                            data: data,
                            dataType: 'json',
                            contentType: false,
                            cache: false,
                            processData: false,
                            error: function(err) {
                //alert('error');
                                    
                alert('error');                
            },
                            success: function(data) {                    
                alert('Updated successfully');      

                window.location = '<?= base_url();?>profile';
                    
            }            
        });


    }
</script>