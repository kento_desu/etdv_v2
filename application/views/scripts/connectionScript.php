<script type="text/javascript">


var connTableSearch = [{
          title: "<center>Profile</center>"
        },{
          title: ""
        },{
          title: ""
        },{
          title: "<center>Profile Name</center>"
        },{
          title: "<center>Profession</center>"
        },{
          title: "<center>Organization</center>"
        },{
          title: "<center>Address</center>"
        },{
          title: '<center>Action</center>'
        },
        ];

  search = $('#search').DataTable({
          columns: connTableSearch,
          dom: 'Bfrtip',        // Needs button container
          select: 'single',
          responsive: true,
          processing: true,
          "pageLength": 8,
          serverSide: true,
          keys: true,
          "searching": true,  
           "bInfo": false,
          ajax:{
                url: "<?= base_url();?>Connection/selectConnectionSearch",
                type: "get"                            
               }, 

          columnDefs: [
          				{     
                        targets: 0,
                        render: function (data) { 

                        return "<center><img src="+"<?= base_url();?>uploads/"+data+".jpg"+ " style='height:80px; width:80px; border-radius:50px;' ></center>";  
                        }
                        // image
					},{
						targets: 1,
                        visible: false,
						render: function (data){

							name = data;
							return data;
						}
					},{
						targets: 2,
                        visible: false,
						render: function (data){
							name = name +' '+data;
							return data;
						}
					},{
            targets: 3, 
            render: function (data){
              name = name + ' '+ data;
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+name+'</button>';
            }
          },{
            targets: 4, 
            render: function (data){
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+data+'</button>';
            }
          },{
            targets: 5, 
            render: function (data){
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+data+'</button>';
            }
          },{
            targets: 6, 
            render: function (data){
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+data+'</button>';
            }
          },
          
          				{
                        
                        targets: 7,
                        // visible: false
                        render: function (data) { 
                        	

                        	data = '<center><button onclick="addConnection('+data+');" class="btn btn-def alert-info"><i class="material-icons">add</i> ADD CONNECTION</center>';

                         

                        	return data; 

                        }

                    
                      }],


          buttons: [
          
        
        ]

        }); // end conn search

function addConnection(data){
	
	$.ajax({
            url: "<?= base_url();?>Connection/connection_ifexist",
            data: {data:data},
            dataType: 'json',
            type: 'post', 
            
            success: function(msg) {
               
              // alert(msg.status);

              if(msg.status){
              	alert('Connecting/Connected Already!');
              }else{
              	// alert('does not exists and we can add now');

              	$.ajax({
		            url: "<?= base_url();?>Connection/connectionRequest",
		            data: {data:data},
		            dataType: 'json',
		            type: 'post', 
		            
		            success: function(msg) {
		               
		              alert('Connection request sent!');
                  
                  connTable.ajax.reload();
                  request.ajax.reload();

		            },
		            error: function(jqXHR, status, error) {
		                
		              alert('error');
		            
		            }
		        });

              }

            },
            error: function(jqXHR, status, error) {
                
              alert('error');
            
            }
        });



	


}

var connTableReq = [{
        title: "<center>Profile</center>"
        },{
          title: ""
        },{
          title: ""
        },{
          title: "<center>Profile Name</center>"
        },{
          title: "<center>Profession</center>"
        },{
          title: "<center>Organization</center>"
        },{
          title: "<center>Email Address</center>"
        },{
          title: '<center>Action</center>'
        },
        ];

        var request = 0;

  request = $('#request').DataTable({
          columns: connTableReq,
          dom: 'Bfrtip',        // Needs button container
          select: 'single',
          responsive: true,
          processing: true,
          "pageLength": 8,
          serverSide: true,
          keys: true,
          "searching": true,
          "bInfo": false,
          ajax:{
                url: "<?= base_url();?>Connection/selectConnectionRequest",
                type: "get"                            
               }, 

          columnDefs: [
          {
                        targets: 0,
                        render: function (data) { 
                          return "<center><img src="+"<?= base_url();?>uploads/"+data+".jpg"+ " style='height:80px; width:80px; border-radius:50px;'></center>"; 
                           }
					},
          {
					
          	targets: 1,
                        visible: false,
						render: function (data){

              

							name = data;
							return data;
						}
					},{
						targets: 2,
                        visible: false,
						render: function (data){
							name = name +' '+data;
							return data;
					}
					},{
            targets: 3, 
            render: function (data){
              name = name + ' '+ data;
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+name+'</button>';
            }
          },{
            targets: 4, 
            render: function (data){
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+data+'</button>';
            }
          },{
            targets: 5, 
            render: function (data){
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+data+'</button>';
            }
          },{
            targets: 6, 
            render: function (data){
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+data+'</button>';
            }
          },
          				{
                        
                        targets: 7,
                        // visible: false
                        render: function (data) { 
                        	
                        	data = '<center><button type="submit"  class="btn btn-def alert-info" data-user-id="'+data+'" onclick="acceptRequest('+data+');"><i class="material-icons">check</i>Accept</button> <button type="submit"  class="btn btn-def alert-danger" data-user-id="'+data+'" onclick="rejectRequest('+data+');"><i class="material-icons">clear</i>Reject</button></center>';

                        	return data; 

                        }

                    
                      }],


          buttons: [
         
        ]

        }); // end conn1


var connTableCol = [{
          title: "<center>Profile</center>"
        },{
          title: ""
        },{
          title: ""
        },{
          title: "<center>Profile Name</center>"
        },{
          title: "<center>Profession</center>"
        },{
          title: "<center>Organization</center>"
        },{
          title: "<center>Email Address</center>"
        },{
          title: "<center>Status</center>"
        },
        ];

  var name;

  connTable = $('#connection1').DataTable({
          columns: connTableCol,
          dom: 'Bfrtip',        // Needs button container
          select: 'single',
          responsive: true,
          processing: true,
          "pageLength": 8,
          serverSide: true,
          keys: true,
          "searching": true,
           "bInfo": false,
          ajax:{
                url: "<?= base_url();?>Connection/selectConnectionList",
                type: "get"                            
               }, 

          columnDefs: [{
                        targets: 0,
                        render: function (data) { 
                          return "<center><img src="+"<?= base_url();?>uploads/"+data+".jpg"+ " style='height:80px; width:80px; border-radius:50px;'></center>"; 
                           }
          },{
						targets: 1,
                        visible: false,
						render: function (data){

							name = data;
							return data;
						}
					},{
						targets: 2,
                        visible: false,
						render: function (data){
							name = name +' '+data;
							return data;
						}
					},{
            targets: 3, 
            render: function (data){
              name = name + ' '+ data;
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+name+'</button>';
            }
          },{
            targets: 4, 
            render: function (data){
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+data+'</button>';
            }
          },{
            targets: 5, 
            render: function (data){
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+data+'</button>';
            }
          },{
            targets: 6, 
            render: function (data){
              return '<button style="position:relative; " class="btn btn-def btn-block" disabled>'+data+'</button>';
            }
          },
          {
						targets: 7,	
						render: function (data){

						if(data == 1){
							data = '<button style="position:relative;  " class="btn btn-def btn-block btn-success">connected</button>';
								
						}else{
							data = '<button style="position:relative;  " class="btn btn-def btn-block btn-warning" >connecting...</button>';
							
						}
							return data;
						}
					},
          {
            targets: 8, 
            visible:false,
            render: function (data){
              return data;
            }
          },
					],


          buttons: [
         
          {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">cancel</i> Cancel Connection',
            name: 'add',        // do not change name
            action: function ( e, dt, button, config ) {

                 
                id = parseInt(dt.row( { selected: true } ).data()[8]);

                cancel_connection(id);

                }
          },
          
        ]

        }); // end conn2

function cancel_connection(id){


  $.ajax({
                url: "<?= base_url();?>Connection/connectionCancel",
                data: {id:id},
                dataType: 'json',
                type: 'post', 
                
                success: function(msg) {
                   
                  alert('Connection Canceled');

                  connTable.ajax.reload();
                  request.ajax.reload();
                  // window.location = '<?= base_url();?>stimuli';

                },
                error: function(jqXHR, status, error) {
                    
                  alert('error');
                
                }
            });

}

function acceptRequest(id){


	$.ajax({
		            url: "<?= base_url();?>Connection/connectionAccept",
		            data: {id:id},
		            dataType: 'json',
		            type: 'post', 
		            
		            success: function(msg) {
		               
		              alert('Accepted');

                  connTable.ajax.reload();
                  request.ajax.reload();
		              // window.location = '<?= base_url();?>stimuli';

		            },
		            error: function(jqXHR, status, error) {
		                
		              alert('error');
		            
		            }
		        });

}


function rejectRequest(id){


          $.ajax({
                url: "<?= base_url();?>Connection/connectionReject",
                data: {id:id},
                dataType: 'json',
                type: 'post', 
                
                success: function(msg) {
                   
                  alert('Rejected');

                  connTable.ajax.reload();
                  request.ajax.reload();
                  // window.location = '<?= base_url();?>stimuli';

                },
                error: function(jqXHR, status, error) {
                    
                  alert('error');
                
                }
            });

}


</script>