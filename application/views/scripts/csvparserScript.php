
<script type="text/javascript">

var fileString;

load_csv_data(0);
var myTable3;

function startRead()
{
  // obtain input element through DOM 
  
  var file = document.getElementById('file').files[0];
  if(file)
  {
    getAsText(file);
  }
}

function getAsText(readFile)
{
  var reader;
  try
  {
    reader = new FileReader();
  }catch(e)
  {
    document.getElementById('output').innerHTML = 
      "Error: seems File API is not supported on your browser";
    return;
  }
  
  // Read file into memory as UTF-8      
  reader.readAsText(readFile, "UTF-8");
  
  // Handle progress, success, and errors
  reader.onprogress = updateProgress;
  reader.onload = loaded;
  reader.onerror = errorHandler;
}

function updateProgress(evt)
{
  if (evt.lengthComputable)
  {
    // evt.loaded and evt.total are ProgressEvent properties
    var loaded = (evt.loaded / evt.total);
    if (loaded < 1)
    {
      // Increase the prog bar length
      // style.width = (loaded * 200) + "px";
      document.getElementById("bar").style.width = (loaded*100) + "%";
    }
  }
}

function loaded(evt)
{
  // Obtain the read file data    
  fileString = evt.target.result;
  // document.getElementById('output').innerHTML = fileString;
  document.getElementById('output').innerHTML = 'Success with no Error!';
    document.getElementById("bar").style.width = 100 + "%";
}

function errorHandler(evt)
{
  if(evt.target.error.code == evt.target.error.NOT_READABLE_ERR)
  {
    // The file could not be read
    document.getElementById('output').innerHTML = "Error reading file..."
  }
}

 


$(document).ready(function() {

 var id=0;
 
 var columnDefs2 = [{
          title: "File #"
        },{
          title: "Date Uploaded"
        },
           {
          title: "Name"
        }
        ];


  // var myTable2;
  myTable2 = $('#example2').DataTable({
          columns: columnDefs2,
          dom: 'Bfrtip',        // Needs button container
          select: 'single',
          responsive: true,
          // altEditor: true,     // Enable altEditor
          processing: true,
          "pageLength": 8,
          serverSide: true,
          keys: true,
          "searching": true,
           "bInfo": false,
          ajax:{
                url: "<?= base_url();?>Stimuli/selectParticipantFile",
                type: "get"                            
               }, 

          columnDefs: [{
                        
                        // targets: 2,
                        // visible: false
                        // render: function (data) { return data+"  hi"; }

                    
                      }],


          buttons: [
          {
            text: '<i class="material-icons">add_circle_outline</i> Upload ',
            name: 'add',        // do not change name
            action: function ( e, dt, button, config ) {
            
                // alert($('#file').val());
                upload_csv();

                // $('#fix-upload').modal('show');

                }
          },
          {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">folder_open</i>',
            name: 'open',        // do not change name
            action: function ( e, dt, button, config ) {

                 
                // alert( dt.row( { selected: true } ).data()[1] );
                  
                id = parseInt(dt.row( { selected: true } ).data()[2]);

                myTable3.destroy();
                myTable3.clear().draw();
                // var myTable3; 
                load_csv_data(id);
                // myTable3.ajax.reload();
                }
          },
          
          {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">border_color</i>',
            name: 'edit',        // do not change name
            action: function ( e, dt, button, config ) {
                
               
                }
          },

          {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">delete_forever</i>',
            name: 'delete',      // do not change name
            action: function ( e, dt, button, config ) {

              id = parseInt(dt.row( { selected: true } ).data()[0]);
              // alert(id);

               delete_csv_data(id);
                
                }

         },
        ]

        });  

     

})

function load_csv_data(id){
      var columnDefs3 = 
        [{
          title: "pid"
        },
        
        {
          title: "x"
        }, 
        {
          title: "y"
        },
        {
          title: "Duration"
        }];

  // myTable3 = $('#example3').DataTable({
  //         "sPaginationType": "full_numbers",
  //         "lengthMenu": [[ -1], [ "All"]],
  //         "scrollY": 280,
  //         "scrollX": true,
  //          "paging": false,
  //          // "dom": '<"top"i>rt<"bottom"flp><"clear">',
  //         columns: columnDefs3,
  //         dom: 'Bfrtip',        // Needs button container
  //         select: 'single',
  //         responsive: true,
  //          processing: true,
  //         serverSide: true,
  //         keys: true,
  //         // "bPaginate": true,
  //         // "bLengthChange": false,
  //         // "bFilter": true,
  //         "bInfo": false,
  //         // "bAutoWidth": false,
  //         "searching":false,
  //         ajax:{
  //               url: "<?php echo site_url('Stimuli/selectFixationData/')?>/" + id,
  //               type: "get"                            
  //              },
  //         columnDefs: [{
                        
  //                       targets: 0,
  //                       "visible": false,
  //                       // render: function (data) { return data+"  hi"; }

                    
  //                     }],      

  //       dom: 'Bfrtip',
  //       buttons: [
  //          'csv'
  //       ]

  //       });
}

function upload_csv(){

if(fileString == undefined){ alert('No file chosen, please chose the fixation (.csv) file.');
}else{

var csv = fileString;

  var lines=csv.split("\n");

  var result = [];

  var headers=lines[0].split(",");

  for(var i=1;i<lines.length;i++){

    var obj = {};
    var currentline=lines[i].split(",");

    for(var j=0;j<headers.length;j++){
      obj[headers[j]] = currentline[j];
    }

    result.push(obj);

  }
  
  var fixation = JSON.stringify(result);
 
  var fix = JSON.parse(fixation);

  console.log(fixation);

   // save_data("", fixation, "");

  // alert();   
  var name =  $( "input[name='participant']" ).val();

  // alert(name);


  if(name != ""){
      $.ajax({
            data: {fixation:fixation, name: name},
            dataType: 'json',
            type: 'post',
            url: '<?= base_url();?>Upload/insert_fixationJSON', 
            error: function (err) {
                alert('error csv file format');
            },

            success: function (msg) {
                
                $( "input[name='ppname']" ).val('');
                $( "input[name='file']" ).val('');

                alert('success');
                myTable2.ajax.reload();
              }
         });
  }else{
        alert('Please enter fixation file name');
  }

}





}//end func

function delete_csv_data(id){

// alert(id);

$.ajax({
            data: {id:id},
            dataType: 'json',
            type: 'post',
            url: '<?= base_url();?>Stimuli/deleteParticipant_File', 
            error: function (err) {
                alert('error');
            },

            success: function (msg) {
                
                alert('removed');
                myTable2.ajax.reload();
              }
         });

}


// 

// 


// $(document).ready(function(){
// $('#load_data').click(function(){



// var data = fileString; 

// var employee_data = data.split(/\r?\n|\r/);
//     var table_data = '<table class="table table-bordered table-striped">';
//     for(var count = 0; count<employee_data.length; count++)
//     {
//      var cell_data = employee_data[count].split(",");
//      table_data += '<tr>';
//      for(var cell_count=0; cell_count<cell_data.length; cell_count++)
//      {
//       if(count === 0)
//       {
//        table_data += '<th>'+cell_data[cell_count]+'</th>';
//       }
//       else
//       {
//        table_data += '<td>'+cell_data[cell_count]+'</td>';
//       }
//      }
//      table_data += '</tr>';
//     }
//     table_data += '</table>';
//     $('#employee_table').html(table_data);


//  });
 
// });
</script>


