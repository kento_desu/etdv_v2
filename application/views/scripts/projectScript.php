<script>
     
  $(document).ready(function() {

        var columnDefs = [{
          title: "Project #"
        }, {
          title: "Project Name"
        }, {
          title: "Description"
        }, {
          title: "Date Created"
        }, 
        // {
        //   title: "Date Modified"
        // }, 
        {
          title: "Status"
        }];

        var myTable;

        myTable = $('#example').DataTable({
          "sPaginationType": "full_numbers",
          columns: columnDefs,
          dom: 'Bfrtip',        // Needs button container
          select: 'single',
          responsive: true,
          // altEditor: true,     // Enable altEditor
          processing: true,
          serverSide: true,
          keys: true,
          "bInfo": false,
          ajax:{
                url: "<?= base_url();?>project/projectList",
                type: "get"                            
               }, 

          columnDefs: [
          {
                        targets: 0,
                        visible: false
          
          },{
                        targets: 4,
                        visible: false
          
          },

          ],


          buttons: [
          {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">folder_open</i> Open',
            name: 'open',        // do not change name
            action: function ( e, dt, button, config ) {

                var id = parseInt(dt.row( { selected: true } ).data()[0]);
                open_project(id);

               
                
                }
          },
          {
            text: '<i class="material-icons">add_circle_outline</i>Create New Project',
            name: 'add',        // do not change name
            action: function ( e, dt, button, config ) {
            
            $('.modal-title').html('New Project');
            $('#submitForm').html('Create'); 

            $('input[name="opt"]').val('add');    
            $('input[name="pname"]').val('');
            $('input[name="desc"]').val('');
            $('#editor-modal').modal('show');   

                }
          },
          {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">border_color</i> Edit',
            name: 'edit',        // do not change name
              action: function ( e, dt, button, config ) {
                  
                  $('.modal-title').html('Edit Project');
                  $('#submitForm').html('Update');

                  $('input[name="opt"]').val('edit');
                  $('input[name="no"]').val(dt.row( { selected: true } ).data()[0]);
                  $('input[name="pname"]').val(dt.row( { selected: true } ).data()[1]);
                  $('input[name="desc"]').val(dt.row( { selected: true } ).data()[2]);
                  $('#editor-modal').modal('show');    

                   
                  id(dt.row( { selected: true } ).data()[0]);

                  }
          },

          {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">delete_forever</i> Delete',
            name: 'delete',      // do not change name
            action: function ( e, dt, button, config ) {


                $('input[name="opt"]').val('delete');
                $('input[name="no"]').val(dt.row( { selected: true } ).data()[0]);
                $('input[name="pname"]').val(dt.row( { selected: true } ).data()[1]);
                $('input[name="desc"]').val(dt.row( { selected: true } ).data()[2]);
                
                
                $('#submitForm').html('Delete');
                
                $('.modal-title').html('Are you sure deleting this project?');
                
                $('#editor-modal').modal('show');    

                }

         },
         {
            extend: 'selected', // Bind to Selected row
            text: '<i class="material-icons">share</i> Share',
            name: 'share',      // do not change name
            action: function ( e, dt, button, config ) {

                var pid = dt.row( { selected: true } ).data()[0];
                $('.project').attr('id', pid);
                get_connection_pid(pid);
                $('.modal-title').html('Share to Connections');
                $('#addrow-modal').modal('show');  


                }
         }]

        });         
      
function get_connection_pid(pid){
  
  // alert(pid);

  $.ajax({
            type: 'GET',
            data: {pid:pid},
            url: '<?= base_url();?>project/connectionShare',
            dataType: 'json',
            success: function(data){

                        for(var i= 0; i < data['data_fetch'].length; i++){
                        
                        // $(".order-list option[id="'8'"]").remove();  

                        var newRow = $("<tr>");
                        var cols = "";
                        var vgrant="Admin";

                        if(data['data_fetch'][i]['grant_status'] == 0){ vgrant = "View-Only";}

                        cols += '<td><input type="text" class="form-control" id="" value="'+data['data_fetch'][i]['fname']+' '+data['data_fetch'][i]['mname']+' '+data['data_fetch'][i]['lname']+'" name="name" disabled/></td>';
                        cols += '<td><input id="grant" value="'+vgrant+'" style="border: none;    background: transparent; margin-top:10px;" disabled></input></td>';
                        
                        cols += '<td><input  style="padding: 0px;" type="button" class="ibtnDel btn btn-md btn-danger "  id="'+data['data_fetch'][i]['rid']+'"  value="Remove"></td>';

                        newRow.append(cols);
                        $("table.order-list").append(newRow);
                        
                      }
     
            },

            error: function(err){
              alert('error');
            }
        });

}


    $("#form").on("submit", function(e) {
      e.preventDefault();

        var url;
        var dataString = $(this).serialize();
      
        if($('input[name="opt"]').val() == 'add'){

            url = '<?= base_url();?>Project/insertProject';
        }else if($('input[name="opt"]').val() == 'edit'){

            var id = $('input[name="no"]').val();
            url = '<?= base_url();?>Project/updateProject';
        }else{

            var id = $('input[name="no"]').val();
            url = '<?= base_url();?>Project/deleteProject';
        }

        $.ajax({
            url: url,
            data: dataString,
            dataType: 'json',
            type: 'post', 
            
            success: function(msg) {
                if(msg.status){
                  // alert('success');
                  $('#editor-modal').modal('hide'); 
                  myTable.ajax.reload();
                
                
                }else{
                  alert('else');
                }
            },
            error: function(jqXHR, status, error) {
                console.log(status + ": " + error);
                alert('error');
            }
        });
        
    });
     
    $("#submitForm").on('click', function() {
        $("#form").submit();
    });


    
    function open_project(id){

      $.ajax({
            url: "<?= base_url();?>Project/openProject_id",
            data: {id:id},
            dataType: 'json',
            type: 'post', 
            
            success: function(msg) {
               
              // alert('success');
              window.location = '<?= base_url();?>stimuli';

            },
            error: function(jqXHR, status, error) {
                
              alert('error');
            
            }
        });

    }

});

   var counter = 0;
   var arr = [];

function add_row(value,id,grant) {
    submit_form(value,grant,id);
    }

function remove_row(){
  $(this).closest("tr").remove();       
        counter -= 1
        // alert('remove');

        var pid = $('.project').attr('id'); // project id
        var id  = $(this).attr('id'); // connection id
        

       $.ajax({
            url: "<?php echo site_url('Project/deleteProjectShare')?>/" + id, // send id
            data: {pid:pid, id:id},
            dataType: 'json',
            type: 'post', 
            
            success: function(msg) {
                if(msg.status){
                  alert('success');
                  // $('#editor-modal').modal('hide'); 
                  // s_table.ajax.reload();
                
                
                }else{
                  alert('else');
                }
            },
            error: function(jqXHR, status, error) {
                console.log(status + ": " + error+ ' projectScript');
                // alert('error');
            }
        }); 

}

$(document).ready(function () {
        
    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1


        var pid = $('.project').attr('id'); // project id
        var id  = $(this).attr('id'); // connection id
        

       $.ajax({
            url: "<?php echo site_url('Project/deleteProjectShare')?>/" + id, // send id
            data: {pid:pid, id:id},
            dataType: 'json',
            type: 'post', 
            
            success: function(msg) {
                if(msg.status){
                  alert('success');
                  // $('#editor-modal').modal('hide'); 
                  // s_table.ajax.reload();
                
                
                }else{
                  alert('else');
                }
            },
            error: function(jqXHR, status, error) {
                console.log(status + ": " + error+ ' projectScript');
                // alert('error');
            }
        }); 
    
    });

});
function remove_table(){

  $(".order-list tbody tr").remove();
        
}

function select_conn(){
  var e = document.getElementById("options");
    var id = e.options[e.selectedIndex].id; //value of rid
    var value = e.options[e.selectedIndex].value; //value of rid
  
  } 

function add_conn(){

  var e = document.getElementById("options");
    var id = e.options[e.selectedIndex].id; //value of rid
    var value = e.options[e.selectedIndex].value; //value of rid
    var pid = $('.project').attr('id'); // project id
    
    var g = document.getElementById("grant");
    var grant = g.options[g.selectedIndex].value; //value of rid
    
    // alert(id+' '+value+' '+pid+' '+grant);
   if(value != 0){
     add_row(value,id,grant); //uncomment this one
   }else{
    alert('Please select Connection');
   }

}

function submit_form(value, grant, id){
 
  var pid = $('.project').attr('id'); // project id
      
        $.ajax({
            data: {to_rid: id, grant_status:grant, pid: pid },
            dataType: 'json',
            type: 'post',
            url: '<?= base_url();?>grant-ifexist', 
            //cache: false, // optional
            error: function (err) {
                alert('error');
            },

            success: function (msg) {
                
                if(msg.status == false){

                  // alert('does not exist, now inserting...');

                  $.ajax({
                    data: {to_rid: id, grant_status:grant, pid: pid  },
                    dataType: 'json',
                    type: 'post',
                    url: '<?= base_url();?>share-connection', 
                    //cache: false, // optional
                    error: function (err) {
                        alert('error');
                    },

                    success: function (msg) {
                        
                        alert('Successfully Shared!');
                        //add row jquery
                        var newRow = $("<tr>");
                        var cols = "";
                        var vgrant="Admin";
                        if(grant == 0){ vgrant = "View-Only";}

                        cols += '<td><input type="text" class="form-control" id="'+id+'" value="'+value+'" name="name' + counter + '" disabled/></td>';
                        cols += '<td><input id="grant'+counter+'" value="'+vgrant+'" style="border: none;    background: transparent; margin-top:10px;" disabled></input></td>';
                        
                        cols += '<td><input style="padding: 0px;" type="button" class="ibtnDel btn btn-md btn-danger " onclick="remove_row('+id+')" id="'+id+'"  value="Remove"></td>';
                        newRow.append(cols);
                        $("table.order-list").append(newRow);
                        counter++;
                       // end

                    }
                });

                }else{

                  alert('Connot add! Please remove the existing connection');
            
                }

               
            }
         });
        
}

function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();

}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}



</script>

