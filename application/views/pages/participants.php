<div style="margin-left: 1%;  overflow-x:hidden;  overflow-y:hidden;" >
    <div class="row">
    <div class="col-md-8 pull-left">
        <div class="card" >
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Participants-Fixation Data</h4>
                <p class="category"></p>
            </div>
            <div class="card-content table-responsive">

                <table class="table table-hover participant">
                    <thead>
                        <tr>
                            <th>Participant #</th>
                            <th>Particpant Name</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>


            </div>
        </div>

    </div>

    <div class="col-md-3 pull-left">


        <div class="card" >
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Fixation Data</h4>
                <p class="category">Select a participant to show the data</p>
            </div>
            <div class="card-content table-responsive">

                <table class="table table-hover fixation-data">
                    <thead>
                        <tr>
                            <th>X</th>
                            <th>Y</th>
                            <th>Duration</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>


            </div>
        </div>

    </div>

</div>

</div>