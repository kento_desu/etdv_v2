<style>
.switch {
    position: relative;
    display: block;
    vertical-align: top;
    width: 100px;
    height: 30px;
    padding: 3px;
    margin: 0 10px 10px 0;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
    border-radius: 18px;
    box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
    cursor: pointer;
    box-sizing:content-box;
}
.switch-input {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    box-sizing:content-box;
}
.switch-label {
    position: relative;
    display: block;
    height: inherit;
    font-size: 10px;
    text-transform: uppercase;
    background: #eceeef;
    border-radius: inherit;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
    box-sizing:content-box;
}
.switch-label:before, .switch-label:after {
    position: absolute;
    top: 50%;
    margin-top: -.5em;
    line-height: 1;
    -webkit-transition: inherit;
    -moz-transition: inherit;
    -o-transition: inherit;
    transition: inherit;
    box-sizing:content-box;
}
.switch-label:before {
    content: attr(data-off);
    right: 11px;
    color: #163d50;
    text-shadow: 0 1px rgba(255, 255, 255, 0.5);
}
.switch-label:after {
    content: attr(data-on);
    left: 11px;
    color: #FFFFFF;
    text-shadow: 0 1px rgba(0, 0, 0, 0.2);
    opacity: 0;
}
.switch-input:checked ~ .switch-label {
    background: #E1B42B;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
}
.switch-input:checked ~ .switch-label:before {
    opacity: 0;
}
.switch-input:checked ~ .switch-label:after {
    opacity: 1;
}


.switch-handle {
    position: absolute;
    top: 4px;
    left: 4px;
    width: 28px;
    height: 28px;
    background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
    background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
    border-radius: 100%;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
}
.switch-handle:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -6px 0 0 -6px;
    width: 12px;
    height: 12px;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
    border-radius: 6px;
    box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
}
.switch-input:checked ~ .switch-handle {
    left: 74px;
    box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
}
 
/* Transition
========================== */
.switch-label, .switch-handle {

    transition: All 0.3s ease;
    -webkit-transition: All 0.3s ease;
    -moz-transition: All 0.3s ease;
    -o-transition: All 0.3s ease;
}


</style>

<style type="text/css">
    
    input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #45a049;
}



</style>

<div class="content" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div  class="card">
                    <div  class="card-header" data-background-color="purple">
                        <h4 class="title">My Projects</h4>
                                    <!-- <p class="category">Here is a subtitle for this table</p> -->
                    </div>
                    <div class="card-content table-responsive" >
                                                      
                            <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0" class="dataTable table table-striped table-hover" id="example">
                            </table>
                          
                    </div>
                </div>
            </div>          
        </div>
    </div>
</div>

<!-- the div that represents the modal dialog -->
<div class="modal fade" id="editor-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
                <div class="modal-body">
                    <form id="form" >
                        <div id="inputs">
                        <input type="hidden" name="opt"><br/>
                        <input type="hidden" name="no"><br/>
                        Project Name: <input type="text" name="pname"><br/>
                        Description:  <input type="text" name="desc"><br/>
                        
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="submitForm" class="btn btn-default">Save</button>
                </div>
            </div>
        </div>
    </div>



<!-- add row -->
<div class="modal fade" id="addrow-modal" data-backdrop="false" role="dialog">
    <div class="modal-dialog">
        <div class="project modal-content" id="">
            <div class="modal-header">
                <button type="button" onclick="remove_table()" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div style="margin-top: 2%; margin-bottom: 1%; margin-left: 5%;">
                 
            <td><select class="myselect" id="options" onchange="select_conn();" style="width:300px;" >
                     <option value="0">Search Connection...</option>
                    <?php 
                    $var = 0;
                    foreach ($tree as $item ) {

                        echo '<option name="'.$tree['data_fetch'][$var]['rid'].'" id="'.$tree['data_fetch'][$var]['rid'].'" value="'.$tree['data_fetch'][$var]['fname'].' '.$tree['data_fetch'][$var]['mname'].'. '.$tree['data_fetch'][$var]['lname'].'">'.$tree['data_fetch'][$var]['fname'].' '.$tree['data_fetch'][$var]['mname'].'. '.$tree['data_fetch'][$var]['lname'].'</option>';
                        
                        $var ++;
                     } 
                     ?>

             </select></td>
            <td><select class="myselect" id="grant" style="width: 100px; margin-top:10px;">
                <option value="0">View-Only</option>
                <option value="1">Admin</option>
            </select>
            </td>

            <td><button onclick="add_conn();" style=" margin-left: 10px; margin-top: 0px;" type="button" class="btn btn-info" ><i class="material-icons">add_circle_outline</i> Add</button>
                    </td>


            </div>


        
                <div class="modal-body">
                 

    <table id="myTable" class=" table order-list">
    
    <thead>
        

        <tr>
            <td>Name</td>
            <td >Privilege</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>

        <tr>
            <div id="csloop"></div>
          
        </tr>
    </tbody>
    
</table>

                </div>
               
            </div>
        </div>
    </div>

<script type="text/javascript">
      $(".myselect").select2({

      });


</script>