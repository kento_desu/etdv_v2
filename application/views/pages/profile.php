<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Edit Profile</h4>
                                    <p class="category">Complete your profile</p>
                                </div>
                                <div class="card-content">
                                    <form id="profile-form">
                                        <div class="row">
                           <div class="col-md-4">
                              <div class="profile form-group label-floating is-focused">
                                 <label class="control-label">Fist Name</label>
                                 <input id="fname" type="text" name="fname" class="form-control" >
                              </div>
                           </div>
                           <div class="col-md-2">
                              <div class="profile form-group label-floating is-focused">
                                 <label class="control-label">M.I</label>
                                 <input id="mname" type="text" name="mname" class="form-control">
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="profile form-group label-floating is-focused">
                                 <label class="control-label">Last Name</label>
                                 <input id="lname" type="text" name="lname" class="form-control">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="profile form-group label-floating is-focused">
                                 <label class="control-label">Profession</label>
                                 <input id="profession" type="text" name="profession" class="form-control">
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="profile form-group label-floating is-focused">
                                 <label class="control-label">Organization</label>
                                 <input id="organization" type="text" name="organization" class="form-control">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="profile form-group label-floating is-focused">
                                 <label class="control-label">Email </label>
                                 <input id="email" type="email" name="email" class="form-control">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="checkbox"><h4 style="color:black;">Change Password <label>
                                                                    <input type="checkbox" name="optionsCheckboxes" id="checkbox" value="scheckbox" ><span class="checkbox-material"><span class="check"></span></span>
                                                                </label></h4>
                                                                
                                                            </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="profile form-group label-floating is-focused">
                                 <label class="control-label">New Password </label>
                                 <input type="password" name="npassword" class="form-control">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class=" profile form-group label-floating is-focused">
                                 <label id="password" class="control-label">Password </label>
                                 <input type="password" name="password" class="form-control" pattern=".{0}|.{8,}"  title="8 chars minimum">
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div id="password" class=" profile form-group label-floating is-focused">
                                 <label class="control-label">Confirm Password </label>
                                 <input type="password" name="cpassword" class="form-control" pattern=".{0}|.{8,}"    title="8 chars minimum">
                              </div>
                           </div>
                        </div>

                                        <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <p></p>
                          <div class="row">
                        <div class="col-md-4">
                            <div class="card card-profile">
                                <div class="card-avatar">
                                    <a href="#pablo">
                                      <!-- -->
                                      <?php 
                                      $this->load->helper('url'); // Load URL Helper for base_url() 
                                      $this->load->helper('html'); // Load HTML Helper for img()
                                      
                                      if($data['status'] == true){
                                        $path = base_url('uploads/'.''.$data['data_fetch'][0]['imgname'].'.jpg'); 
                                        
                                      
                                        }else{
                                        $path = base_url('uploads/user.jpg'); 
                                         
                                        }
                                        echo anchor($path, img($path));


                                       ?>

                                    </a>
                                </div>
                                <div class="content">
                                    <h6 class="category text-gray">Profile Picture </h6>
                                    <!-- <h4 class="card-title">Alec Thompson</h4>
                                     -->
                                     <br>
                                    <center>
                                      <form method="post" id="upload_form" enctype="multipart/form-data">
                                        <input type="file" class="btn btn-primary btn-round" name="image_file" id="image_file">
                                        
                                    <input type="submit" name="upload" class="btn btn-primary btn-round" id="upload" value="Upload">

                                    </form>
                                    </center>

                                  <!--   <a href="#pablo" class="btn btn-primary btn-round"><i class="material-icons">file_upload</i> Change Image</a>
                                -->
                                </div>
                                <p></p>
                            </div>
                        </div>
                    </div>

                      

                    </div>

                </div>
            </div>

            <script type="text/javascript">
              
              $(function () {
                  $('input[name="npassword"]').hide();
                  $('input[name="password"]').hide();
                  $('input[name="cpassword"]').hide();

                  //show it when the checkbox is clicked
                  $('input[name="optionsCheckboxes"]').on('click', function () {
                      if ($(this).prop('checked')) {
                          $('input[name="npassword"]').fadeIn();
                          $('input[name="password"]').fadeIn();
                          $('input[name="cpassword"]').fadeIn();

                  
                  $('#fname').prop('disabled', true);
                  $('#mname').prop('disabled', true);
                  $('input[name="lname"]').prop('disabled', true);
                  $('input[name="organization"]').prop('disabled', true);
                  $('input[name="profession"]').prop('disabled', true);
                  $('input[name="email"]').prop('disabled', true);

                      } else {
                          $('input[name="npassword"]').hide();
                          $('input[name="password"]').hide();
                          $('input[name="cpassword"]').hide();

                  $('#fname').prop('disabled', false);
                  $('#mname').prop('disabled', false);
                  $('input[name="lname"]').prop('disabled', false);
                  $('input[name="organization"]').prop('disabled',  false);
                  $('input[name="profession"]').prop('disabled', false);
                  $('input[name="email"]').prop('disabled', false);
                      }
                  });
              });
            </script>