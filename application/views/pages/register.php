<style type="text/css">
    body{
   background-image: url("https://www.wallpaperflare.com/static/611/336/840/cup-of-coffee-laptop-office-macbook-wallpaper.jpg");
   background-size:     cover;                      /* <------ */
   background-repeat:   no-repeat;
   background-position: center center;
   }
   
   div.well{
   height: 250px;
   } 
   .Absolute-Center {
   margin: auto;
   margin-top: 100px;
   position: absolute;
   top: 0; left: 0; bottom: 0; right: 0;
   }
   .Absolute-Center.is-Responsive {
   width: 100%; 
   height: 50%;
   min-width: 600px;
   max-width: 400px;
   padding: 40px;
   }
   #logo-container{
   margin: auto;
   margin-bottom: 10px;
   width:200px;
   height:30px;
   background-image:url('http://placehold.it/200x30/000000/ffffff/&text=BRAND+LOGO');
   }
   p{
   color: white;
   }

   .lds-facebook {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-facebook div {
  display: inline-block;
  position: absolute;
  left: 6px;
  width: 13px;
  background: #fdd;
  animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
}
.lds-facebook div:nth-child(1) {
  left: 6px;
  animation-delay: -0.24s;
}
.lds-facebook div:nth-child(2) {
  left: 26px;
  animation-delay: -0.12s;
}
.lds-facebook div:nth-child(3) {
  left: 45px;
  animation-delay: 0;
}
@keyframes lds-facebook {
  0% {
    top: 6px;
    height: 51px;
  }
  50%, 100% {
    top: 19px;
    height: 26px;
  }
}



</style>
<nav class="navbar navbar-transparent navbar-absolute">
   <div class="container-fluid">
      <div class="collapse navbar-collapse">
         <ul class="nav navbar-nav navbar-left">
            <li>
               <a href="#">
                  <p><i class="material-icons">multiline_chart</i> Eye Tracking Data Visualizer</p>
               </a>
            </li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
            <li>
               <a href="<?= base_url();?>register">
                  <p><i class="material-icons">fingerprint</i>Register</p>
               </a>
            </li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
            <li>
               <a href="<?= base_url();?>login">
                  <p><i class="material-icons">lock_open</i>Login</p>
               </a>
            </li>
         </ul>
      </div>
   </div>
</nav>
<div class="layer">
   <div class="container">
      <div class="row">
         <div class="Absolute-Center is-Responsive">
            <!-- <div id="logo-container"></div> -->
            <div class="col-sm-12 col-md-12 col-md-offset-1">
               <div class="card " style="">
                  <div class="card-header" data-background-color="purple">
                     <h4 class="title" style="text-align: center; height: 20px;">Register</h4>
                  </div>
                  <div class="card-content table-responsive" style="padding-left: 50px; padding-right: 50px;">
                     <form  id="register-form" >
                    
                        <div class="row">
                           <div class="col-md-4">
                              <div class="form-group label-floating is-focused">
                                 <label class="control-label">Fist Name</label>
                                 <input type="text" name="fname" class="form-control">
                              </div>
                           </div>
                           <div class="col-md-2">
                              <div class="form-group label-floating is-focused">
                                 <label class="control-label">M.I</label>
                                 <input type="text" name="mname" class="form-control">
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group label-floating is-focused">
                                 <label class="control-label">Last Name</label>
                                 <input type="text" name="lname" class="form-control">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group label-floating is-focused">
                                 <label class="control-label">Profession</label>
                                 <input type="text" name="profession" class="form-control">
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group label-floating is-focused">
                                 <label class="control-label">Organization</label>
                                 <input type="text" name="organization" class="form-control">
                              </div>  
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group label-floating is-focused">
                                 <label class="control-label">Email </label>
                                 <input type="email" name="email" class="form-control">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="password form-group label-floating is-focused">
                                 <label class="control-label">Password </label>
                                 <input type="password" name="password" class="form-control" pattern=".{0}|.{8,}"   required title="8 chars minimum">
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="password form-group label-floating is-focused">
                                 <label class="control-label">Confirm Password </label>
                                 <input type="password" name="cpassword" class="form-control" pattern=".{0}|.{8,}"   required title="8 chars minimum">
                              </div>
                           </div>
                        </div>
                     
                     <div class="checkbox">
                      
                     </div>
                     <div class="form-group">
                        <button type="submit"  class="btn btn-def btn-block"><div id="loader">Register</div></button>
                     </div>
                     </form>        
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>