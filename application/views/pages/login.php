<style type="text/css">
   body{
   background-image: url("https://www.wallpaperflare.com/static/611/336/840/cup-of-coffee-laptop-office-macbook-wallpaper.jpg");
   background-size:     cover;                      /* <------ */
   background-repeat:   no-repeat;
   background-position: center center;
   }
   
   div.well{
   height: 250px;
   } 
   .Absolute-Center {
   margin: auto;
   margin-top: 100px;
   position: absolute;
   top: 0; left: 0; bottom: 0; right: 0;
   }
   .Absolute-Center.is-Responsive {
   width: 100%; 
   height: 50%;
   min-width: 600px;
   max-width: 400px;
   padding: 40px;
   }
   #logo-container{
   margin: auto;
   margin-bottom: 10px;
   width:200px;
   height:30px;
   background-image:url('http://placehold.it/200x30/000000/ffffff/&text=BRAND+LOGO');
   }
   p{
   color: white;
   }

   .lds-facebook {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-facebook div {
  display: inline-block;
  position: absolute;
  left: 6px;
  width: 13px;
  background: #fdd;
  animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
}
.lds-facebook div:nth-child(1) {
  left: 6px;
  animation-delay: -0.24s;
}
.lds-facebook div:nth-child(2) {
  left: 26px;
  animation-delay: -0.12s;
}
.lds-facebook div:nth-child(3) {
  left: 45px;
  animation-delay: 0;
}
@keyframes lds-facebook {
  0% {
    top: 6px;
    height: 51px;
  }
  50%, 100% {
    top: 19px;
    height: 26px;
  }
}


</style>
<nav class="navbar navbar-transparent navbar-absolute">
   <div class="container-fluid">
      <div class="collapse navbar-collapse">
         <ul class="nav navbar-nav navbar-left">
            <li>
               <a href="#">
                  <h5><p><i class="material-icons">multiline_chart</i> Eye Tracking Data Visualization Tool</p></h5>
               </a>
            </li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
            <li>
               <a href="<?= base_url();?>register">
                  <h5><p><i class="material-icons">fingerprint</i>Register</p>
               </h5></a>
            </li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
            <li>
               <a href="<?= base_url();?>login">
               <h5>   <p><i class="material-icons">lock_open</i>Login</p>
               </h5></a>
            </li>
         </ul>
      </div>
   </div>
</nav>
<div class="layer">
   <div class="container">
      <div class="row">
         <div class="Absolute-Center is-Responsive">
            <!-- <div id="logo-container"></div> -->
            <div class="col-sm-12 col-md-10 col-md-offset-1">
               <div class="card ">
                  <div class="card-header" data-background-color="purple">
                     <h4 class="title" style="text-align: center; height: 20px;">Login</h4>
                  </div>
                  <div class="card-content table-responsive">
                     <form  id="login-form">
                        <div class="form-group input-group ">
                           <span class="input-group-addon"><i class="material-icons">email</i></span>
                           <input class="form-control" type="email" name='email' placeholder="Email" required autofocus />          
                        </div>
                        <div class="form-group input-group">
                           <span class="input-group-addon"><i class="material-icons">lock_outline</i></span>
                           <input class="form-control" type="password" name='password' placeholder="Password" required autofocus />     
                        </div>
                        
                        <div id="reloader" class="form-group">
                           <button type="submit"  class="btn btn-def btn-block"><div id="loader">Login</div></button>
                           
                        </div>
                        
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



