<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Connections</h4>
                        <p></p>
                        <p></p>

                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">


                            <li class="nav-item active">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">My Connections</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Connection Requests</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-search-tab" data-toggle="pill" href="#pills-search" role="tab" aria-controls="pills-search" aria-selected="false">Search Connections</a>
                            </li>
                            <!-- <p class="category">Here is a subtitle for this table</p> -->
                    </div>


                    <div class="card-content table-responsive">
                        <div style=" max-height: 500px;">



                            </ul>
                            <div class="tab-content" id="pills-tabContent">

                                <div class="tab-pane fade " id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                
                                <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0" class="dataTable table table-striped table-hover" id="request">
                                </table>

                                </div>

                                <div class="tab-pane fade active in" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                
                                <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0" class="dataTable table table-striped table-hover" id="connection1">
                                </table>
                                </div>

                                 <div class="tab-pane fade" id="pills-search" role="tabpanel" aria-labelledby="pills-search-tab">
                                
                                <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0" class="dataTable table table-striped table-hover" id="search">
                                </table>
                                </div>

                    
                            </div>

                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>





<!-- the div that represents the modal dialog -->
<div class="modal fade" id="editor-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
                <div class="modal-body">
                    <form id="form" >
                        <div id="inputs">
                        <input type="hidden" name="opt"><br/>
                        <input type="hidden" name="no"><br/>
                        Project Name: <input type="text" name="pname"><br/>
                        Description:  <input type="text" name="desc"><br/>
                        
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="submitForm" class="btn btn-default">Submit</button>
                </div>
            </div>
        </div>
    </div>



