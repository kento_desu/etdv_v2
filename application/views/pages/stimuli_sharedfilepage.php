<style type="text/css">
    .modal-dialogg {
        width: 100%;
        height: 100%;
    }
    
    .modal-content {
        height: 100%;
        border-radius: 0;
    }
    
    .stepwizard-step p {
        margin-top: 0px;
        color: #666;
    }
    
    .stepwizard-row {
        display: table-row;
    }
    
    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }
    
    .stepwizard-step button[disabled] {
        /*opacity: 1 !important;
    filter: alpha(opacity=100) !important;*/
    }
    
    .stepwizard .btn.disabled,
    .stepwizard .btn[disabled],
    .stepwizard fieldset[disabled] .btn {
        opacity: 1 !important;
        color: #bbb;
    }
    
    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-index: 0;
    }
    
    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }
    
    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }

        input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #45a049;
}
    


</style>

<!-- edit -->
<div class="modal fade" id="editor-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
                <div class="modal-body">
                    <form id="form">
                        <div id="inputs">
                        <input type="hidden" name="opt"><br/>
                        <input type="hidden" name="no"><br/>
                        Stimulus Name: <input type="text" name="pname"><br/>
                        Description:   <input type="text" name="desc"><br/>
                        <!-- x_resolution:  <input type="text" name="xres"><br/>
                        y_resolution:  <input type="text" name="yres"><br/> -->
                        

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="submitForm" class="btn btn-default">Submit</button>
                </div>
            </div>
        </div>
    </div>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <div class="row">
                            
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" >

                                 
                        
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <h4 class="title"> <h3><p>File Name: <?php echo $pname; echo '</p></h3><p>Description: '; echo $pdesc; echo'<p>Date: '; echo $pdate; ?></h4>
                        
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right">
                        <h4 class="title"> <h3><p> <?php 
                            echo "";
                            echo $sname; 
                            echo '</p></h3><p>Date/Time: '; 
                            echo $sdate.'<p>'; 
                            echo '</p>'; 
                         
                            echo $status; ?></h4>
                        </div>
                         <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 pull-right" style="padding-right: 10%;">
                        
                     <?php echo '<img src="'.base_url().'/uploads/'.$file['data_fetch'][0]['imgname']. '.jpg " style="height:120px; width:120px; margin-top:2px; border-radius:5%;">' ?>
                  </div> 

                            </div>

                            
                        </div>

                    </div>

                    <div class="card-content table-responsive">
                        <div style=" max-height: 380px;">

                            <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0" class="dataTable table table-striped table-hover" id="s_table">

                            </table>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- edit modal -->

<div class="modal fade" id="stimuliModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialogg" style="margin: 0px; padding: 0px;">

        <div class="modal-body">

            <!--  -->
            <div class="container" style="width: 50%; background-color: white; border-radius: 5px;">
                <h4 class="modal-title" style="text-align: center; padding-top: 20px; padding-bottom: 10px;" id="myModalLabel"><div style="color: black;">Setting-up New Stimulus Data</div></h4>

                <div class="stepwizard">

                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step col-xs-7">

                            <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                            <p><small>Stimulus Basic Info</small></p>
                        </div>
                        <div class="stepwizard-step col-xs-3">
                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                            <p><small>Area of Interest (AOI) Setup</small></p>
                        </div>
                        
                    </div>
                </div>

                <form role="form" id="form-id">
                    <div class="panel panel-primary setup-content" id="step-1">

                        <div class="panel-heading">
                            <h3 class="panel-title">Stimulus Information</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group is-focused">
                                <label class="control-label">Stimulus Name</label>
                                <input maxlength="100" name="sname" type="text" required="required" class="form-control" placeholder="Stimulus Name" />
                            </div>
                            <div class="form-group is-focused">
                                <label class="control-label">Description</label>
                                <input maxlength="100" type="text" name="sdesc" required="required" class="form-control" placeholder="Description" />
                            </div>
                            <div class="form-group is-focused">
                                <label class="control-label">Screen Resolution</label>
                                
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <input maxlength="100" type="number" name="x" required="required" class="form-control" placeholder="x-resolution  by" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <input maxlength="100" type="number" name="y" required="required" class="form-control" placeholder="y-resolution" />
                                    </div>
                                </div>

                                
                            </div>
                            <input type="hidden" id="btnOpt" name="btnOpt" value=""></input>
                            <button type="button" class="btn btn-default pill-left" data-dismiss="modal">Cancel</button><button  class="btn btn-default nextBtn pull-right" type="button">Next</button><button onclick="saveStimulusInfo()" class="btn btn-primary nextBtn pull-right" type="button">Save</button>
                        </div>
                    </div>

                    <div class="panel panel-primary setup-content" id="step-2">
                        <div class="panel-heading">
                            <h3 class="panel-title">Destination</h3>
                        </div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">

                                        <div style="padding-left: 30%;"><button onclick="editStimuli();" class="btn btn-default pull-left" style="margin-top: 0px; margin-bottom: 5px;" type="button">Edit Stimulus (AOI)</button></div>
    
                                        

                                    </div>
                                </div>
                            </div>

                            <button type="button" class="btn btn-default pill-left" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-primary nextBtn pull-right" data-dismiss="modal" type="button">Finish</button>
                        </div>
                    </div>

                 
                </form>
                </div>

            </div>

        </div>
    </div>






    <div class="modal fade" id="editStimuliModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialogg" style="margin: 0px; padding: 0px; background-color: #434343; overflow:scroll; 
    background-image:       linear-gradient(0deg, transparent 24%, rgba(255, 255, 255, .05) 25%, rgba(255, 255, 255, .05) 26%, transparent 27%, transparent 74%, rgba(255, 255, 255, .05) 75%, rgba(255, 255, 255, .05) 76%, transparent 77%, transparent), linear-gradient(90deg, transparent 24%, rgba(255, 255, 255, .05) 25%, rgba(255, 255, 255, .05) 26%, transparent 27%, transparent 74%, rgba(255, 255, 255, .05) 75%, rgba(255, 255, 255, .05) 76%, transparent 77%, transparent);
  height:100%;
  background-size:50px 50px;">

            <div class="modal-body" style="padding-top: 0px;">

                <div id="dim"></div>

                <div class="pop" id="pop-local">
                    <p class="pop-title">LOAD LOCAL IMAGE</p>
                    <div class="pop-content">
                        <!-- <input type="file" id="pop-local-input"> -->
                        
                       <form method="post" id="upload_form" enctype="multipart/form-data">

                            <input type="file" name="image_file" id="pop-local-input">
                            <br>
                            <p>

                                <input class="pop-btn-confirm" type="submit" name="upload" id="upload" value="Upload">
                                <input class="pop-btn-cancel" type="button" name="cancel" id="cancel" value="Cancel">

                        </form>
                        
                        <!-- <form method="post" id="upload_form" enctype="multipart/form-data">

                            <input type="file" name="image_file" id="pop-local-input">
                            <br>
                            <p>

                                <input class="pop-btn-confirm" type="submit" name="upload" id="upload" value="Upload">
                                <input class="pop-btn-cancel btn-default" type="submit" name="cancel" id="cancel" value="Cancel">

                        </form> -->
                        
                       <!--  <form method="post" id="upload_form" enctype="multipart/form-data">
                                        <input type="file" class="btn  btn-primary btn-round" name="image_file" id="image_file">
                                        
                                    <input type="submit" name="upload" class="btn btn-primary btn-round" id="upload" value="Upload">

                        </form> -->
                       

                    </div>
                    <!--  <div class="pop-btn">
        <div class="pop-btn-confirm">LOAD</div>
        <div class="pop-btn-cancel">CANCEL</div>
    </div> -->
                </div>

                <div class="pop" id="pop-url">
                    <p class="pop-title">LINK IMAGE URL</p>
                    <div class="pop-content">
                        <input type="text" id="pop-url-input">
                    </div>
                    <div class="pop-btn">
                        <div class="pop-btn-confirm">LINK</div>
                        <div class="pop-btn-cancel">CANCEL</div>
                    </div>
                </div>

                <div class="pop" id="pop-code">
                    <p class="pop-title">CODE GENERATED</p>
                    <div class="pop-btn">
                        <div class="pop-btn-copy" id="pop-btn-copy-a">SHOW MARKUP AS <em>&lt;A&gt; TAG</em> FORM</div>
                        <div class="pop-btn-copy" id="pop-btn-copy-im">SHOW MARKUP AS <em>IMAGE MAP</em> FORM</div>
                        <div class="pop-btn-cancel _full">CLOSE</div>
                    </div>
                </div>

                <div class="pop" id="pop-codegen-a">
                    <p class="pop-title">&lt;A&gt; TAG FORM</p>
                    <div class="pop-content">
                        <p></p>
                    </div>
                    <div class="pop-btn-cancel _back">BACK</div>
                    <div class="pop-btn-cancel">CLOSE</div>
                </div>

                <div class="pop" id="pop-codegen-im">
                    <p class="pop-title">IMAGE MAP FORM</p>
                    <div class="pop-content">
                        <p></p>
                    </div>
                    <div class="pop-btn-cancel _back">BACK</div>
                    <div class="pop-btn-cancel">CLOSE</div>
                </div>

                <div class="pop" id="pop-info">
                    <p class="pop-title">APP INFORMATION</p>
                    <div class="pop-content">
                        <p>
                            <em class="pop-content-alert">&#9888; This app works on IE10+ only.</em>
                            <strong>Easy Image Mapper (v1.2.0)</strong>
                            <br> Author: Inpyo Jeon
                            <br> Contact: inpyoj@gmail.com
                            <br> Website: <a class="_hover-ul" href="https://github.com/1npy0/easy-mapper" target="_blank">GitHub Repository</a>
                        </p>
                    </div>
                    <div class="pop-btn-cancel _full">CLOSE</div>
                </div>

                <div id="gnb">
                    <a id="gnb-title" href="easy-mapper.html" data-dismiss="modal">&#10060; CLOSE</a>

                    <ul id="gnb-menu">
                        <li id="gnb-menu-source">
                            <!-- <span>UPLOAD STIMULUS &#9662;</span> -->
                            <!-- <ul class="gnb-menu-sub"> -->
                            <li id="gnb-menu-local">UPLOAD STIMULUS</li>
                            <!-- <li id="gnb-menu-url">URL</li> -->
                            <!-- </ul> -->
                        </li>
                        <li id="gnb-menu-measure">
                            <span>MEASURE &#9662;</span>
                            <ul class="gnb-menu-sub _toggle">
                                <li id="gnb-menu-drag" class="_active">DRAG<em>&nbsp;&#10003;</em></li>
                                <li id="gnb-menu-click">CLICK<em>&nbsp;&#10003;</em></li>
                            </ul>
                        </li>
                       
                        <li id="gnb-menu-clear">
                            <span>CLEAR</span>
                        </li>
                        <li id="gnb-menu-generate">
                            <span>SAVE</span>
                        </li>
                        <!-- <li id="gnb-menu-generate">
            <span>SAVE</span>
        </li> -->
                        <!-- <li id="gnb-menu-info">
                            <span>?</span>
                        </li> -->
                    </ul>
                </div>

                <div id="workspace">

                    <div id="workspace-ruler">
                        <div id="workspace-ruler-x">
                            <div id="workspace-ruler-x-2"></div>
                            <div id="workspace-ruler-x-1"></div>
                        </div>
                        <div id="workspace-ruler-y">
                            <div id="workspace-ruler-y-2"></div>
                            <div id="workspace-ruler-y-1"></div>
                        </div>
                    </div>

                    <div id="workspace-img-wrap">

                        <div id="grid-x1" class="grid-1"></div>
                        <div id="grid-y1" class="grid-1"></div>
                        <div id="grid-x2" class="grid-2"></div>
                        <div id="grid-y2" class="grid-2"></div>
                        <span id="grid-coords"></span>
                    </div>

                </div>

            </div>
        </div>
    </div>