<div class="col-md-12">
    <div class="row">
        
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="card" >
            <div class="card-header" data-background-color="purple">
                <h4>Participants Filter</h4>
                <p class="category"></p>
            </div>
            <div class="card-content table-responsive">

            <!-- <h3>Participant Filters</h3> -->
            <?php if( ! empty($participants)) : ?>
                <?php foreach($participants as $participant):?>
                    <div class="form-check">
                        
                        <input type="checkbox" class=" form-check-input chart-action " data-id="<?php echo $participant['ppid'];?>" name="ppid" id="participant<?php echo $participant['ppid'];?>" checked>
                        <label class="form-check-label" for="participant<?php echo $participant['ppid'];?>" style="color:black;"><?php echo $participant['name']?></label>
                    </div>
                <?php endforeach;?>
            <?php else: ?>
                <h4>No Participants available</h4>
            <?php endif; ?>


            </div>
        </div>
        </div>  <!---->
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                <div class="row">
                    <div class="col-md-3"> 
                        <label style="color: white;"><h4>Stimulus Name: </h4><span class="stimuli-name"><?php  echo $sname;?></span></label>
                    </div>
                    <div class="col-md-3"> 
                        <label style="color: white;"><h4>Description: </h4><span class="stimuli-desc"><?php  echo $sdesc;?></span></label>
                    </div>
                    <div class="col-md-3"> 
                        <label style="color: white;"><h4>Date: </h4><span class="stimuli-date"><?php  echo $pdate;?></span></label>
                    </div>
                </div>
            </div>
                
            </div>

            <div class="row" style="margin: auto 0;">
                 <div style="padding-left: 0;padding-right:0;" class="btn-group btn-group-toggle metric-group col-md-3" data-toggle="buttons">
                    <label class="btn btn-primary metric-category active">
                        <input type="radio" name="metrics-category" id="nof-category" autocomplete="off" checked> NOF
                    </label>
                    <label class="btn btn-primary metric-category">
                        <input type="radio" name="metrics-category" id="dff-category" autocomplete="off"> DFF
                    </label>
                    <label class="btn btn-primary metric-category">
                        <input type="radio" name="metrics-category" id="tct-category" autocomplete="off"> TCT
                    </label>
                </div>
                 <div class="col-md-3 alert-warning" style="margin-top: 6px; margin-left: 13%;" >
                <center><h4 class="title" "></h4></center>
                </div>

                
                <div class="col-md-3 pull-right" style="padding: 0px;">
                    <label class="btn btn-primary export-pdf ">
                        <i class = "material-icons ">picture_as_pdf</i> Export Pdf
                    </label>
                </div>
               
            </div>
            <div class="row chart-container">
                <canvas id="summary-chart" width="400" height="200"></canvas>
            </div>
        </div>
    </div>
</div>